<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
  <_name>Affine Distortion</_name>
  <id>su-v/org.inkscape.effect.image_affine</id>

  <dependency type="executable" location="extensions">image_affine.py</dependency>
  <dependency type="executable" location="extensions">image_lib/common.py</dependency>
  <dependency type="executable" location="extensions">image_lib/transform.py</dependency>
  <dependency type="executable" location="extensions">inkex.py</dependency>
  <dependency type="executable" location="extensions">cubicsuperpath.py</dependency>
  <dependency type="executable" location="extensions">simpletransform.py</dependency>

  <param name="tab" type="notebook">
    <page name="options" _gui-text="Options">
      <param name="mode" type="enum" _gui-text="Distortion method:">
        <_item value="affine">Affine</_item>
      </param>
      <param name="reverse_subpaths" type="boolean" _gui-text="Reverse order of helper paths">false</param>
      <param name="reverse_source" type="boolean" _gui-text="Reverse direction of source path">false</param>
      <param name="reverse_dest" type="boolean" _gui-text="Reverse direction of destination path">false</param>
      <param name="virtual_pixel" type="enum" _gui-text="Virtual Pixel:">
        <_item value="transparent">Transparent</_item>
        <_item value="black">Black</_item>
        <_item value="gray">Gray</_item>
        <_item value="white">White</_item>
        <_item value="background">Background</_item>
        <_item value="constant">Constant</_item>
        <_item value="dither">Dither</_item>
        <_item value="edge">Edge</_item>
        <_item value="mirror">Mirror</_item>
        <_item value="random">Random</_item>
        <_item value="tile">Tile</_item>
        <_item value="mask">Mask</_item>
        <_item value="horizontal_tile">HorizontalTile</_item>
        <_item value="vertical_tile">VerticalTile</_item>
        <_item value="horizontal_tile_edge">HorizontalTileEdge</_item>
        <_item value="vertical_tile_edge">VerticalTileEdge</_item>
        <_item value="checker_tile">CheckerTile</_item>
        <_item value="undefined">Undefined</_item>
      </param>
      <param name="matte_color" type="enum" _gui-text="Matte Color:">
        <_item value="transparent">Transparent</_item>
        <_item value="black">Black</_item>
        <_item value="gray">Gray</_item>
        <_item value="white">White</_item>
        <_item value="red">Red</_item>
        <_item value="green">Green</_item>
        <_item value="blue">Blue</_item>
      </param>
      <param name="wrap" type="enum" gui-text="Wrap result:">
        <_item value="no">No</_item>
        <_item value="delete">Delete helper path</_item>
      </param>
    </page>
    <page name="help" _gui-text="Help">
      <param name="distort_affine_head" type="description" appearance="header">Affine Distortion</param>
      <param name="distort_affine_desc" type="description" _gui-description="... moving a list of at least 3 or more sets of control points. Ideally 3 sets are given allowing the image to be linearly scaled, rotated, sheared, and translated, according to those three points. More than 3 sets given control point pairs is least squares fitted to best match a linear affine distortion. If only 2 control point pairs are given a two point image translation rotation and scaling is performed, without any possible shearing, flipping or changes in aspect ratio to the resulting image.">Distort the image linearly ...</param>
      <param name="distort_affine_ref_desc" type="string" _gui-text="See also:">http://www.imagemagick.org/Usage/distorts/#affine</param>
      <param name="distort_affine_usage_head" type="description" appearance="header">Usage</param>
      <param name="distort_affine_usage_desc" type="description" _gui-description="Source and destination control points are the corresponding nodes of two selected helper paths. The two helper paths need to have the same number of nodes.">See tooltip.</param>
    </page>
  </param>

  <effect needs-document="true" needs-live-preview="true">
    <menu-tip>Applies Affine transformation to a bitmap image based on 2 helper paths.</menu-tip>
    <object-type>all</object-type>
    <effects-menu>
      <submenu _name="Modify Image"/>
    </effects-menu>
  </effect>

  <script>
    <command reldir="extensions" interpreter="python">image_affine.py</command>
  </script>

</inkscape-extension>
