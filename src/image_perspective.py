#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
image_perspective.py - perspectively transform a bitmap image based on a
                       quadrilateral helper path

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


From original PIL documentation:

    im.transform(size, PERSPECTIVE, data) image ⇒ image

    im.transform(size, PERSPECTIVE, data, filter) image ⇒ image

    Applies a perspective transform to the image, and places the result
    in a new image with the given size.

    Data is a 8-tuple (a, b, c, d, e, f, g, h) which contains the
    coefficients for a perspective transform. For each pixel (x, y)
    in the output image, the new value is taken from a position
    (a x + b y + c)/(g x + h y + 1), (d x + e y + f)/(g x + h y + 1)
    in the input image, rounded to nearest pixel.

    This function can be used to change the 2D perspective of the
    original image.

.. Source:
   http://effbot.org/imagingbook/image.htm#tag-Image.Image.transform

"""
# pylint: disable=too-many-locals
# pylint: disable=too-many-branches

# standard library
import inkex
from image_lib import common as modimg


class ImagePerspective(modimg.ImageModifier1):
    """ImageModifier-based class to apply Perspective/Envelope."""

    def __init__(self):
        """Init base class and add options for ImagePerspective class."""
        modimg.ImageModifier1.__init__(self)
        # 4-point transformation
        self.OptionParser.add_option("--mode",
                                     action="store", type="string",
                                     dest="mode", default="perspective",
                                     help="Distortion method")
        self.OptionParser.add_option("--orig_dest",
                                     action="store", type="string",
                                     dest="orig_dest", default="1",
                                     help="Origin (top-left) of quadrilateral")
        self.OptionParser.add_option("--reverse_dest",
                                     action="store", type="inkbool",
                                     dest="reverse_dest", default=False,
                                     help="Reverse direction of quadrilateral")
        self.OptionParser.add_option("--fit",
                                     action="store", type="inkbool",
                                     dest="fit", default=True,
                                     help="Fit result to quadrilateral")
        self.OptionParser.add_option("--wrap",
                                     action="store", type="string",
                                     dest="wrap", default="no",
                                     help="Wrap result")

    def modify_image(self, img_node, path, points=4, subs=1):
        """Modify image content based on helper path and options."""
        modimg.select_imaging_module(self.options.imaging_module)
        image, dest = modimg.check_req(img_node, path,
                                       points, subs, alpha=True)
        fit = self.options.fit
        if image is not None and dest is not None:
            # image dimensions
            width, height = image.size
            scale_x, scale_y = modimg.get_image_scale(image, img_node)

            # compensate transforms for dest
            mat_p2i = modimg.transform_path_to_image(path, img_node,
                                                     image, fit)
            modimg.mat.apply_to(mat_p2i, dest)

            # prepare dest for pointlist
            source = [(0, 0), (width, 0), (width, height), (0, height)]
            dest = modimg.csp_to_points(dest, 0, points)

            # start and direction of destination quadrilateral
            if self.options.reverse_dest:
                dest.reverse()
            for _ in range(int(self.options.orig_dest) - 1):
                dest.append(dest.pop(0))

            # normalize dest to image coords,
            # fit output size to dest bbox
            if fit:
                x_coords, y_coords = zip(*dest)
                x_min = min(*x_coords)
                y_min = min(*y_coords)
                dest = [(x - x_min, y - y_min) for x, y in dest]
                width = int(max(*x_coords) - x_min)
                height = int(max(*y_coords) - y_min)

            # distort image
            if modimg.USE_PIL:
                if self.options.mode != 'perspective':
                    inkex.errormsg(self.not_with_pil)
                elif modimg.HAVE_NUMPY:
                    coeffs = modimg.find_perspective_coeffs(source, dest)
                    image = image.transform((width, height),
                                            modimg.ImagePIL.PERSPECTIVE,
                                            coeffs,
                                            modimg.ImagePIL.BICUBIC)
                else:
                    inkex.errormsg(self.requires_numpy)
            elif modimg.USE_WAND:
                method = self.options.mode
                if fit and method != 'perspective':
                    # TODO: find documentation about 'bestfit' flag with
                    # 'BilinearForward' distort method
                    image.resize(width, height)
                    # TODO: compare results with different IM resize
                    # methods image.distort('resize', [width, height])
                    source = [(0, 0), (width, 0),
                              (width, height), (0, height)]
                ptlist = [p[i][j]
                          for i in range(points)
                          for p in [source, dest]
                          for j in range(2)]
                image.virtual_pixel = self.options.virtual_pixel
                image.matte_color = modimg.ColorWand(self.options.matte_color)
                # use ImageMagick's 'bestfit' flag to avoid resizing the
                # image first
                image.distort(method, ptlist, fit)
                if fit and method == 'perspective':
                    # compensate ImageMagick's larger viewport (+2px)
                    # with 'bestfit', see:
                    # www.imagemagick.org/Usage/distorts/#distort_bestfit
                    x_min -= 1
                    y_min -= 1

            # save distorted image
            modimg.save_image(img_node, image)

            # adjust position and size of modified source image,
            # compensate image scale
            if fit:
                img_node.set('width', str(image.size[0] / scale_x))
                img_node.set('height', str(image.size[1] / scale_y))
                img_node.set('x', str(x_min / scale_x))
                img_node.set('y', str(y_min / scale_y))
                # apply preserved transform from path
                modimg.mat.apply_copy_from(path, img_node)

            # post-processing
            self.wrap_result(img_node, path, self.options.wrap)


if __name__ == '__main__':
    ME = ImagePerspective()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
