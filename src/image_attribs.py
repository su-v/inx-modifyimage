#!/usr/bin/env python
'''
image_attribs.py - adjust image attributes which don't have global
                   GUI options yet

Tool for Inkscape 0.91 to adjust rendering of drawings with linked
or embedded bitmap images created with older versions of Inkscape
or third-party applications.

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'''

# local library
from image_lib.common import ImageAttributer
import simplestyle


def change_attribute(node, attribute):
    """Change attritbute or style of node."""
    for key, value in attribute.items():
        if key == 'preserveAspectRatio':
            # set presentation attribute
            if value != "unset":
                node.set(key, str(value))
            else:
                if node.get(key):
                    del node.attrib[key]
        elif key == 'image-rendering':
            node_style = simplestyle.parseStyle(node.get('style'))
            if key not in node_style:
                # set presentation attribute
                if value != "unset":
                    node.set(key, str(value))
                else:
                    if node.get(key):
                        del node.attrib[key]
            else:
                # set style property
                if value != "unset":
                    node_style[key] = str(value)
                else:
                    del node_style[key]
                node.set('style', simplestyle.formatStyle(node_style))
        else:
            pass


class ImageRendering(ImageAttributer):
    """ImageModifier-based class to adjust <image> SVG attributes.

    Supported style properties:

        image-rendering

        Properties can be set directly on the <image> element, or
        on parent container elements (including SVGRoot) due to
        style inheritance.

    Supported presentation attributes:

        preserveAspectRatio

        This attribute needs to be set for the <image> element
        directly, and cannot be defined in a global (or local)
        context.
    """

    def __init__(self):
        """Init base class, add options for ImageRendering class."""
        ImageAttributer.__init__(self)
        # main options
        self.OptionParser.add_option("--fix_scaling",
                                     action="store", type="inkbool",
                                     dest="fix_scaling", default=True,
                                     help="")
        self.OptionParser.add_option("--fix_rendering",
                                     action="store", type="inkbool",
                                     dest="fix_rendering", default=False,
                                     help="")
        self.OptionParser.add_option("--aspect_ratio",
                                     action="store", type="string",
                                     dest="aspect_ratio", default="none",
                                     help="Value for 'preserveAspectRatio'")
        self.OptionParser.add_option("--aspect_clip",
                                     action="store", type="string",
                                     dest="aspect_clip", default="unset",
                                     help="optional 'meetOrSlice' value")
        self.OptionParser.add_option("--image_rendering",
                                     action="store", type="string",
                                     dest="image_rendering", default="unset",
                                     help="Value for 'image-rendering'")

    def get_params(self):
        """Return parameter values valid for all attribute changes."""
        attr_val = []
        attr_dict = {}
        if self.options.tab == '"tab_basic"':
            attr_dict['preserveAspectRatio'] = (
                "none" if self.options.fix_scaling else "unset")
            attr_dict['image-rendering'] = (
                "optimizeSpeed" if self.options.fix_rendering else "unset")
        elif self.options.tab == '"tab_attribute"':
            attr_val = [self.options.aspect_ratio]
            if self.options.aspect_clip != "unset":
                attr_val.append(self.options.aspect_clip)
            attr_dict['preserveAspectRatio'] = ' '.join(attr_val)
        elif self.options.tab == '"tab_property"':
            attr_dict['image-rendering'] = self.options.image_rendering
        elif self.options.tab == '"help"':
            pass
        else:  # unknown tab
            pass
        return attr_dict

    def modify_image(self, node, params, points=None, subs=None):
        """Modify image-rendering and preserveAspectRatio attributes."""
        if params:
            change_attribute(node, params)


if __name__ == '__main__':
    ME = ImageRendering()
    ME.affect()


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
