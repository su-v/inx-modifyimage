<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
  <_name>Crop to Clip</_name>
  <id>su-v/org.inkscape.effect.image_crop_clip</id>

  <dependency type="executable" location="extensions">image_crop_clip.py</dependency>
  <dependency type="executable" location="extensions">image_lib/common.py</dependency>
  <dependency type="executable" location="extensions">image_lib/transform.py</dependency>
  <dependency type="executable" location="extensions">inkex.py</dependency>
  <dependency type="executable" location="extensions">cubicsuperpath.py</dependency>
  <dependency type="executable" location="extensions">simpletransform.py</dependency>

  <param name="tab" type="notebook">
    <page name="options" _gui-text="Options">
      <param name="scope" type="enum" _gui-text="Scope:">
        <_item value="selected_only">Change only selected image(s)</_item>
        <_item value="in_selection">Change all images in selection</_item>
        <_item value="in_document">Change all images in document</_item>
      </param>
      <param name="crop_to_ancestor_clip" type="boolean" _gui-text="Crop to next clip on parent group">false</param>
      <param name="wrap" type="enum" gui-text="Wrap result:">
        <_item value="no">No</_item>
        <_item value="clip_release">Release clip</_item>
        <_item value="clip_release_delete">Remove clip</_item>
        <_item value="crop_box">Append cropbox as path</_item>
        <_item value="crop_box_group">Group cropbox and cropped image</_item>
      </param>
    </page>
    <page name="help" _gui-text="Help">
      <param name="crop_head" type="description" appearance="header">Crop</param>
      <param name="crop_desc" type="description" _gui-description="Crops to the bounding box (in the image's coordinate system) of the clipPath of a clipped bitmap image.">Crops image to clip path.</param>
    </page>
  </param>
  <param name="imaging_module" type="enum" gui-text="Python Imaging module:">
    <_item value="default">Default</_item>
    <_item value="pil">PIL/Pillow</_item>
    <_item value="wand">Wand (ImageMagick)</_item>
  </param>
  <param name="debug" type="boolean" _gui-text="Debug">false</param>

  <effect needs-document="true" needs-live-preview="true">
    <menu-tip>Crop content of clipped bitmap image with bbox of clipPath</menu-tip>
    <object-type>all</object-type>
    <effects-menu>
      <submenu _name="Modify Image"/>
    </effects-menu>
  </effect>

  <script>
    <command reldir="extensions" interpreter="python">image_crop_clip.py</command>
  </script>

</inkscape-extension>
