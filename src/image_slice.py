#!/usr/bin/env python
"""
image_slice.py - Slice bitmap image based on numeric input, guides
                 or helper paths

Copyright (C) 2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# standard library
from math import ceil

# local library
from image_lib import common as modimg
from guillotine import Guillotine
import simplestyle


def get_h_guide_pos_in_bbox(doc, bbox):
    """Make a sorted list of all horizontal guide positions in bbox."""
    ymin, ymax = bbox[2:4]
    guide_doc = Guillotine()
    guide_doc.document = doc
    root = guide_doc.document.getroot()
    page_height = guide_doc.unittouu(root.attrib['height'])
    horizontals = [ymin]
    for guide_pos in guide_doc.get_all_horizontal_guides():
        # NOTE: guides are still in desktop coordinates!
        guide_pos = page_height - float(guide_pos)
        if guide_pos > ymin and guide_pos < ymax:
            horizontals.append(guide_pos)
    horizontals.append(ymax)
    horizontals.sort()
    return horizontals


def get_v_guide_pos_in_bbox(doc, bbox):
    """Make a sorted list of all vertical guide positions in bbox."""
    xmin, xmax = bbox[:2]
    guide_doc = Guillotine()
    guide_doc.document = doc
    verticals = [xmin]
    for guide_pos in guide_doc.get_all_vertical_guides():
        guide_pos = float(guide_pos)
        if guide_pos > xmin and guide_pos < xmax:
            verticals.append(guide_pos)
    verticals.append(xmax)
    verticals.sort()
    return verticals


def get_img_render_mode(node):
    """Return value of image-rendering attribute or property or None."""
    key = 'image-rendering'
    node_style = simplestyle.parseStyle(node.get('style'))
    if key in node_style:
        # return style property value
        return node_style[key]
    elif key in node.attrib:
        # return presentation attribute value
        return node.get(key)
    else:
        # nothing has been set
        return None


def set_img_render_mode(node, mode=None):
    """Set image-rendering mode of imge alement passed as argument."""
    key = 'image-rendering'
    node_style = simplestyle.parseStyle(node.get('style'))
    if key in node_style:
        # update style property if present
        if mode is not None:
            node_style[key] = mode
        else:
            del node_style[key]
        node.set('style', simplestyle.formatStyle(node_style))
    if key in node.attrib:
        # update presentation attribute if present
        if mode is not None:
            node.set(key, mode)
        else:
            del node.attrib[key]
    if key not in node.attrib and key not in node_style:
        # finally, set presentation attribute if nothing is set
        if mode is not None:
            node.set(key, mode)


class ImageSlice(modimg.ImageModifier):
    """ImageModifier-based class to slice bitmap image."""

    def __init__(self):
        """Init base class and add options for ImageSlice class."""
        modimg.ImageModifier.__init__(self)
        # slice
        self.OptionParser.add_option("--rows",
                                     action="store", type="int",
                                     dest="rows", default=2,
                                     help="Number of rows")
        self.OptionParser.add_option("--cols",
                                     action="store", type="int",
                                     dest="cols", default=2,
                                     help="Number of columns")
        self.OptionParser.add_option("--pixelgrid",
                                     action="store", type="inkbool",
                                     dest="pixelgrid", default=True,
                                     help="Slice to image pixelgrid")
        self.OptionParser.add_option("--wrap",
                                     action="store", type="string",
                                     dest="wrap", default="no",
                                     help="Wrap result")
        self.OptionParser.add_option("--keep_orig",
                                     action="store", type="inkbool",
                                     dest="keep_orig", default=True,
                                     help="Keep original bitmap image")
        # tabs
        self.OptionParser.add_option("--nb_geom",
                                     action="store", type="string",
                                     dest="nb_geom",
                                     help="Current tab for geometry input")

    def get_slices_rows_cols(self, image):
        """Compute slicer rects in image coords based on rows/columns."""
        # pylint: disable=too-many-locals
        slicer_path = []
        rows = self.options.rows
        columns = self.options.cols
        width, height = image.size
        h_spacing = int(width / columns)
        v_spacing = int(height / rows)
        for row in range(rows):
            y = row * v_spacing
            if row < rows - 1:
                v_last = v_spacing
            else:
                v_last = height - y
            for column in range(columns):
                x = column * h_spacing
                if column < columns - 1:
                    h_last = h_spacing
                else:
                    h_last = width - x
                rect_d = 'M {0},{1} h {2} v {3} h -{2} Z'.format(
                    x, y, h_last, v_last)
                modimg.showme(rect_d)
                slicer_path.append(modimg.cubicsuperpath.parsePath(rect_d)[0])
        if len(slicer_path):
            return slicer_path
        else:
            return None

    def get_slices_input_geom(self, img_node, image):
        """Compute slicer rects in image coords based on input data."""
        # pylint: disable=too-many-locals
        slicer_path = []
        img_x = float(img_node.get('x', 0))
        img_y = float(img_node.get('y', 0))
        width = float(img_node.get('width', 0))
        height = float(img_node.get('height', 0))
        columns = self.options.cols
        rows = self.options.rows
        h_spacing = width / columns
        v_spacing = height / rows
        for row in range(rows):
            y = row * v_spacing
            for column in range(columns):
                x = column * h_spacing
                rect_d = 'M {0},{1} h {2} v {3} h -{2} Z'.format(
                    img_x + x, img_y + y, h_spacing, v_spacing)
                modimg.showme(rect_d)
                slicer_path.append(modimg.cubicsuperpath.parsePath(rect_d)[0])
        if len(slicer_path):
            # Transform paths to image coords.
            mat_i2i = modimg.mat_img_node_to_image(img_node, image)
            modimg.mat.apply_to(mat_i2i, slicer_path)
            return slicer_path
        else:
            return None

    def get_slices_guides(self, img_node, path, image):
        """Compute slicer rects in image coords based on h/v guides."""
        # get transform from path coords to SVGRoot
        mat = modimg.mat.absolute(path)
        # get bbox of path transformed to SVGRoot
        bbox = modimg.mat.st.computeBBox([path], mat)
        # get positions of hor/ver guides within bbox
        h_pos = get_h_guide_pos_in_bbox(self.document, bbox)
        v_pos = get_v_guide_pos_in_bbox(self.document, bbox)
        # construct slicer path (for dest) with 1 sub-path per slice
        slicer_path = []
        for i in range(len(h_pos)-1):
            for j in range(len(v_pos)-1):
                rect_d = 'M {0},{1} h {2} v {3} h -{2} Z'.format(
                    v_pos[j], h_pos[i],
                    v_pos[j+1] - v_pos[j], h_pos[i+1] - h_pos[i])
                modimg.showme(rect_d)
                slicer_path.append(modimg.cubicsuperpath.parsePath(rect_d)[0])
        # transform slicer_path back to path coords
        modimg.mat.apply_to(modimg.mat.invert(mat), slicer_path)
        if len(slicer_path):
            # Finally, transform path to image coords (for dest)
            mat_p2i = modimg.transform_path_to_image(path, img_node, image)
            modimg.mat.apply_to(mat_p2i, slicer_path)
            return slicer_path
        else:
            return None

    def get_slices_paths(self, img_node, path, image):
        """Compute slicer rects in ? coords based on selected paths."""
        # pylint: disable=no-self-use
        if path is not None:
            # Transform path to image coords (for dest)
            csp = modimg.cubicsuperpath.parsePath(path.get('d'))
            mat_p2i = modimg.transform_path_to_image(path, img_node, image)
            modimg.mat.apply_to(mat_p2i, csp)
            return csp
        else:
            return None

    def modify_image(self, img_node, path, points=None, subs=None):
        """Modify image content based on helper path."""
        # pylint: disable=too-many-locals
        modimg.select_imaging_module(self.options.imaging_module)
        image = modimg.prep_image(img_node)
        # get compund path with sub-path per slice
        if self.options.nb_geom == '"custom"':
            if self.options.pixelgrid:
                # Precise mode (spacing in ints, last row/column fits)
                dest = self.get_slices_rows_cols(image)
            else:
                # Overlapping mode (by 1px):
                dest = self.get_slices_input_geom(img_node, image)
        elif self.options.nb_geom == '"guides"':
            dest = self.get_slices_guides(img_node, path, image)
        elif self.options.nb_geom == '"paths"':
            dest = self.get_slices_paths(img_node, path, image)
        else:
            dest = None
        if image is not None and dest is not None:
            # original image scale
            scale_x, scale_y = modimg.get_image_scale(image, img_node)

            # compensate transforms
            mat_i2p = modimg.transform_image_to_path(image, img_node, path)

            # insertion index for newly created image slices
            img_index = img_node.getparent().index(img_node) + 1

            # loop through dest (path compound of slicer rects / paths)
            for i, slicer in enumerate(dest):
                # prepare crop points
                ptlist = modimg.csp_to_points(dest, i, points)

                # slicer bbox in image coords (don't exceed image area)
                x_coords, y_coords = zip(*ptlist)
                x_min = max(min(*x_coords), 0)
                y_min = max(min(*y_coords), 0)
                x_max = min(max(*x_coords), image.size[0])
                y_max = min(max(*y_coords), image.size[1])
                # pointlist from bbox of slicer
                pointlist = [int(j) for j in [x_min, y_min,
                                              ceil(x_max), ceil(y_max)]]
                modimg.showme(pointlist)

                # crop image using ptlist
                if modimg.USE_WAND:
                    img_format = "keep"
                    # crop an image by [left:right, top:bottom]
                    # with maintaining the original
                    slice_img = image[pointlist[0]:pointlist[2],
                                      pointlist[1]:pointlist[3]]
                elif modimg.USE_PIL:
                    img_format = image.format
                    slice_img = image.crop(pointlist)

                # create new image element for slice
                slice_node = modimg.create_img_node()
                # set image-rendering attribute or property accordingly
                set_img_render_mode(slice_node, get_img_render_mode(img_node))
                # insert image slice into document
                img_node.getparent().insert(img_index+i, slice_node)
                # save cropped image data as new <image> element
                modimg.save_image(slice_node, slice_img, img_format)

                # adjust position and size of slicer image,
                # compensate image scale
                modimg.mat.apply_copy_from(img_node, slice_node)
                slice_node.set('width', str(slice_img.size[0] / scale_x))
                slice_node.set('height', str(slice_img.size[1] / scale_y))
                slice_node.set('x', str(float(img_node.get("x", "0")) +
                                        int(x_min) / scale_x))
                slice_node.set('y', str(float(img_node.get("y", "0")) +
                                        int(y_min) / scale_y))

                # optionally clip new <image> element with slicer
                if (self.options.wrap.startswith('clip') or
                        self.options.wrap.startswith('group')):
                    slice_path = modimg.draw_cropbox(path, [slicer])
                    modimg.mat.apply_to(mat_i2p, slice_path)
                    self.wrap_result(slice_node, slice_path, self.options.wrap)

            # cleanup combined helper paths
            path.getparent().remove(path)

            # optionally delete original bitmap image
            if not self.options.keep_orig:
                img_node.getparent().remove(img_node)


if __name__ == '__main__':
    ME = ImageSlice()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
