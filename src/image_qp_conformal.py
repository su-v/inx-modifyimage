#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
image_qp_conformal.py - parameterized quadratic conformal distortion of
                        a bitmap image based on a helper path (8 nodes)

Copyright (C) 2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

def q_orig(s,t,a,b,c,d,e,f,g,h):
  p = 1.0*((1/4)*b-(1/2)*s*b+(1/2)*s*s*b+(1/2)*t*t*d+(1/2)*t*d+(1/4)*e+(1/4)*g+
    t*t*s*s*((1/4)*g+(1/4)*b+(1/4)*d+(1/4)*e)-(1/2)*t*t*s*g+(1/2)*s*s*g+
    (1/2)*s*g-s*s*((1/4)*g+(1/4)*b+(1/4)*d+(1/4)*e)-t*t*((1/4)*g+(1/4)*b+
    (1/4)*d+(1/4)*e)-(1/2)*t*t*s*s*e+(1/2)*t*s*s*e+(1/4)*t*s*f+(1/4)*t*s*s*f+
    (1/4)*t*t*s*f+(1/4)*t*t*s*s*f-(1/4)*t*t*s*c-(1/4)*t*s*s*c+
    (1/4)*t*t*s*s*c+(1/4)*t*s*c-(1/2)*t*t*s*s*g-(1/2)*t*e+(1/2)*t*t*e-
    (1/2)*t*t*s*s*b+(1/2)*t*t*s*b+(1/4)*d-(1/2)*t*s*s*d-(1/2)*t*t*s*s*d+
    (1/4)*t*t*s*s*a-(1/4)*t*s*a-(1/4)*t*t*s*a+(1/4)*t*s*s*a-(1/4)*t*s*s*h+
    (1/4)*t*t*s*s*h+(1/4)*t*t*s*h-(1/4)*t*s*h)
  return p

.. Source:
   http://benpaulthurstonblog.blogspot.com/2015/12/parameterized-quadratic-conformal.html

"""
# local library
import inkex
from image_lib import common as modimg


def qpc(s, t, a, b, c, d, e, f, g, h):
    """Return pixel position for Quadratic Conformal transformation."""
    # pylint: disable=invalid-name
    # pylint: disable=too-many-arguments
    p = 1.0 * (
        0.25*b -
        0.5*s*b +
        0.5*s*s*b +
        0.5*t*t*d +
        0.5*t*d +
        0.25*e +
        0.25*g +
        t*t*s*s*(0.25*g + 0.25*b + 0.25*d + 0.25*e) -
        0.5*t*t*s*g +
        0.5*s*s*g +
        0.5*s*g -
        s*s*(0.25*g + 0.25*b + 0.25*d + 0.25*e) -
        t*t*(0.25*g + 0.25*b + 0.25*d + 0.25*e) -
        0.5*t*t*s*s*e +
        0.5*t*s*s*e +
        0.25*t*s*f +
        0.25*t*s*s*f +
        0.25*t*t*s*f +
        0.25*t*t*s*s*f -
        0.25*t*t*s*c -
        0.25*t*s*s*c +
        0.25*t*t*s*s*c +
        0.25*t*s*c -
        0.5*t*t*s*s*g -
        0.5*t*e +
        0.5*t*t*e -
        0.5*t*t*s*s*b +
        0.5*t*t*s*b +
        0.25*d -
        0.5*t*s*s*d -
        0.5*t*t*s*s*d +
        0.25*t*t*s*s*a -
        0.25*t*s*a -
        0.25*t*t*s*a +
        0.25*t*s*s*a -
        0.25*t*s*s*h +
        0.25*t*t*s*s*h +
        0.25*t*t*s*h -
        0.25*t*s*h
    )
    return p


class ImageQPConformal(modimg.ImageModifier1):
    """ImageModifier-based class to apply QP Conformal transformation."""

    def __init__(self):
        """Init base class and add options for ImageQPConformal class."""
        modimg.ImageModifier1.__init__(self)
        # 4-point transformation
        self.OptionParser.add_option("--mode",
                                     action="store", type="string",
                                     dest="mode", default="qp_conformal",
                                     help="Distortion method")
        self.OptionParser.add_option("--orig_dest",
                                     action="store", type="string",
                                     dest="orig_dest", default="1",
                                     help="Origin (top-left) of quadrilateral")
        self.OptionParser.add_option("--reverse_dest",
                                     action="store", type="inkbool",
                                     dest="reverse_dest", default=False,
                                     help="Reverse direction of quadrilateral")
        self.OptionParser.add_option("--fit",
                                     action="store", type="inkbool",
                                     dest="fit", default=True,
                                     help="Fit result to quadrilateral")
        self.OptionParser.add_option("--oversample",
                                     action="store", type="int",
                                     dest="oversample", default=1,
                                     help="Oversample")
        self.OptionParser.add_option("--wrap",
                                     action="store", type="string",
                                     dest="wrap", default="no",
                                     help="Wrap result")

    def apply_qp_conformal(self, image, c, output_size):
        """Apply custom distortion, return output image."""
        # pylint: disable=invalid-name
        output = modimg.ImagePIL.new("RGBA", output_size)
        oversample = self.options.oversample
        width, height = image.size
        # TODO: Investigate using PIL sequences to improve performance:
        # Image.getdata() to get image data, process sequences in loop
        # and update the output en-bloc after the loop with Image.putdata()
        for i in range(0, oversample * width):
            for j in range(0, oversample * height):
                s = (2.0 / (oversample * width)) * i - 1
                t = -((2.0 / (oversample * height)) * j - 1)
                color = image.getpixel(
                    (i / float(oversample), j / float(oversample)))
                px = qpc(s, t,
                         c[0][0], c[7][0], c[6][0], c[1][0],
                         c[5][0], c[2][0], c[3][0], c[4][0])
                py = qpc(s, t,
                         c[0][1], c[7][1], c[6][1], c[1][1],
                         c[5][1], c[2][1], c[3][1], c[4][1])
                try:
                    output.putpixel((int(px), int(py)), color)
                except IndexError:
                    pass
        return output

    def modify_image(self, img_node, path, points=8, subs=1):
        """Modify image content based on helper path and options."""
        # pylint: disable=too-many-locals

        modimg.select_imaging_module('pil')
        image, dest = modimg.check_req(img_node, path,
                                       points, subs, alpha=True)
        fit = self.options.fit
        if image is not None and dest is not None:
            # image dimensions
            width, height = image.size
            scale_x, scale_y = modimg.get_image_scale(image, img_node)

            # compensate transforms for dest
            mat_p2i = modimg.transform_path_to_image(
                path, img_node, image, fit)
            modimg.mat.apply_to(mat_p2i, dest)

            # prepare dest for pointlist
            dest = modimg.csp_to_points(dest, 0, points)

            # start and direction of destination quadrilateral
            if self.options.reverse_dest:
                dest.reverse()
            for _ in range(int(self.options.orig_dest) - 1):
                dest.append(dest.pop(0))

            # normalize dest to image coords,
            # fit output size to dest bbox
            # TODO: get approx. bbox of E_Bezier (?)
            if fit:
                x_coords, y_coords = zip(*dest)
                x_min = min(*x_coords)
                y_min = min(*y_coords)
                dest = [(x - x_min, y - y_min) for x, y in dest]
                width = int(max(*x_coords) - x_min)
                height = int(max(*y_coords) - y_min)

            # distort image
            if modimg.USE_WAND:
                inkex.errormsg(self.not_with_wand)
            elif modimg.USE_PIL:
                if self.options.mode != 'qp_conformal':
                    inkex.errormsg(self.not_with_pil)
                else:
                    image = self.apply_qp_conformal(image, dest,
                                                    (width, height))

            # save distorted image
            modimg.save_image(img_node, image)

            # adjust position and size of modified source image,
            # compensate image scale
            if fit:
                img_node.set('width', str(image.size[0] / scale_x))
                img_node.set('height', str(image.size[1] / scale_y))
                img_node.set('x', str(x_min / scale_x))
                img_node.set('y', str(y_min / scale_y))
                # apply preserved transform from path
                modimg.mat.apply_copy_from(path, img_node)

            # post-processing
            self.wrap_result(img_node, path, self.options.wrap)


if __name__ == '__main__':
    ME = ImageQPConformal()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
