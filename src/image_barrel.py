#!/usr/bin/env python
"""
image_barrel.py - barrel distortion of a bitmap image

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# local library
import inkex
from image_lib import common as modimg


class ImageBarrel(modimg.ImageModifier):
    """ImageModifier-based class to apply Barrel distortion to bitmap."""

    def __init__(self):
        """Init base class and add options for ImageBarrel class."""
        modimg.ImageModifier.__init__(self)
        # geometry custom input
        self.OptionParser.add_option("--A",
                                     action="store", type="float",
                                     dest="A", default=0.0,
                                     help="A")
        self.OptionParser.add_option("--B",
                                     action="store", type="float",
                                     dest="B", default=0.0,
                                     help="B")
        self.OptionParser.add_option("--C",
                                     action="store", type="float",
                                     dest="C", default=0.0,
                                     help="C")
        self.OptionParser.add_option("--D",
                                     action="store", type="float",
                                     dest="D", default=1.0,
                                     help="D")

    def modify_image(self, img_node, path, points=None, subs=1):
        """Modify image content based on the options."""
        if modimg.USE_PIL:
            inkex.errormsg(self.not_with_pil)
            image = None
        else:
            image, _ = modimg.check_req(img_node, path, points, subs)
        if image is not None:
            # transformation method
            method = 'barrel'

            # distortion parameters
            pointlist = []
            pointlist.append(self.options.A)
            pointlist.append(self.options.B)
            pointlist.append(self.options.C)
            pointlist.append(self.options.D)

            image.virtual_pixel = self.options.virtual_pixel
            image.matte_color = modimg.ColorWand(self.options.matte_color)
            image.distort(method, pointlist)
            modimg.save_image(img_node, image)


if __name__ == '__main__':
    ME = ImageBarrel()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
