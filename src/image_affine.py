#!/usr/bin/env python
"""
image_affine.py - affine distortion of a bitmap image based on
                  sub-paths of a helper path

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# local library
import inkex
from image_lib import common as modimg


class ImageAffine(modimg.ImageModifier2):
    """ImageModifier-based class to apply Affine distortion to bitmap."""

    def __init__(self):
        """Init base class and add options for ImageAffine class."""
        modimg.ImageModifier2.__init__(self)
        # affine transformation
        self.OptionParser.add_option("--mode",
                                     action="store", type="string",
                                     dest="mode", default="affine",
                                     help="Distortion method")
        self.OptionParser.add_option("--reverse_subpaths",
                                     action="store", type="inkbool",
                                     dest="reverse_subpaths", default=False,
                                     help="Reverse direction of helper paths")
        self.OptionParser.add_option("--reverse_source",
                                     action="store", type="inkbool",
                                     dest="reverse_source", default=False,
                                     help="Reverse direction of source")
        self.OptionParser.add_option("--reverse_dest",
                                     action="store", type="inkbool",
                                     dest="reverse_dest", default=False,
                                     help="Reverse direction of destination")
        self.OptionParser.add_option("--wrap",
                                     action="store", type="string",
                                     dest="wrap", default="no",
                                     help="Wrap result")

    def modify_image(self, img_node, path, points=None, subs=2):
        """Modify image content based on helper path and options."""
        if modimg.USE_PIL:
            inkex.errormsg(self.not_with_pil)
            image = dest = None
        else:
            image, dest = modimg.check_req(img_node, path, points, subs)
        if image is not None and dest is not None:
            # compensate transforms for dest
            mat_p2i = modimg.transform_path_to_image(path, img_node, image)
            modimg.mat.apply_to(mat_p2i, dest)

            # prepare source, dest for pointlist
            if self.options.reverse_subpaths:
                dest.reverse()
            source = modimg.csp_to_points(dest, 0, points)
            dest = modimg.csp_to_points(dest, 1, points)

            # start and direction of source helper path
            if self.options.reverse_source:
                source.reverse()

            # start and direction of destination helper path
            if self.options.reverse_dest:
                dest.reverse()

            # transformation method
            method = self.options.mode

            points = len(source)
            pointlist = [p[i][j]
                         for i in range(points)
                         for p in [source, dest]
                         for j in range(2)]
            image.virtual_pixel = self.options.virtual_pixel
            image.matte_color = modimg.ColorWand(self.options.matte_color)
            image.distort(method, pointlist)
            modimg.save_image(img_node, image)

            # post-processing
            path.getparent().remove(path)


if __name__ == '__main__':
    ME = ImageAffine()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
