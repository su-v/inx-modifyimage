#!/usr/bin/env python
"""
image_reset.py - extension to reset dimensions of bitmap images in current
                 selection

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=wrong-import-order

# standard library
try:
    from fractions import Fraction  # (requires Python >= 2.6)
    HAVE_FRACTIONS = True
except ImportError:
    HAVE_FRACTIONS = False

# compat
import six

# local library
import inkex
from image_lib import common as modimg


# helper functions

def showme(msg):
    """Print debug output only if global DEBUG variable is set."""
    if modimg.DEBUG:
        inkex.debug(msg)


def get_pil_exif_field(exif, field):
    """Return Exif data for field key."""
    # pylint: disable=invalid-name
    if modimg.HAVE_PIL and modimg.PIL_EXIF_TAGS:
        if exif is not None:
            for (k, v) in six.iteritems(exif):
                if modimg.PIL_EXIF_TAGS.get(k) == field:
                    return v
    return None


def pil_exif_rational_to_float(alist):
    """Convert Exif data tuple with 'rational' format to float."""
    if isinstance(alist, tuple) and len(alist) == 2:
        if alist[1] != 0:
            return float(alist[0]) / float(alist[1])
    return None


def wand_exif_rational_to_float(string):
    """Convert Exif data string with 'rational' format to float."""
    # Exif data:
    #   "rational" means a fractional value, it contains 2 signed /
    #   unsigned long integer value, and the first represents the
    #   numerator, the second, the denominator.
    if HAVE_FRACTIONS:  # Python >= 2.6
        try:
            return float(Fraction(string))
        except ZeroDivisionError:
            pass
    else:
        alist = []
        try:
            alist = [float(i) for i in string.split('/')]
        except ValueError:
            pass
        if len(alist) == 2:
            if alist[1] != 0:
                return alist[0] / alist[1]
    return None


class ImageResolution():
    """Retrieve image resolution from available image data."""

    def __init__(self):
        """
        This class can only be used as parent class along with another
        parent class which is based on inkex.Effect() - otherwise
        self.unittouu() is undefined.
        """
        # FIXME: sort out this mess so that ImageResolution can be reused
        # in other extensions via import and inhertiance.
        # pylint: disable=no-member

    def read_pil_exif(self, img, img_res):
        """Read Exif data for image resolution via PIL imaging module."""
        # pylint: disable=protected-access
        showme("Trying exif data...")
        res_x = res_y = res_u = None
        if hasattr(img, '_getexif'):
            exif = img._getexif()
            res_x = pil_exif_rational_to_float(
                get_pil_exif_field(exif, 'XResolution'))
            res_y = pil_exif_rational_to_float(
                get_pil_exif_field(exif, 'YResolution'))
            unit = get_pil_exif_field(exif, 'ResolutionUnit')
            if res_x and res_y:
                if unit == 2:
                    scale = self.unittouu('1in')
                    res_u = 'dots per inch'
                elif unit == 3:
                    scale = self.unittouu('1cm')
                    res_u = 'dots per cm'
                else:  # unknown (1)
                    res_u = None
            if res_x and res_y and res_u:
                img_res['ok'] = True
                img_res['scale'] = scale
                img_res['res_x'] = res_x
                img_res['res_y'] = res_y
                img_res['res_u'] = res_u

    def read_pil_jfif(self, img, img_res):
        """Read jfif data for image resolution via PIL imaging module."""
        showme("Trying jfif data...")
        res_x = res_y = res_u = None
        if 'jfif_density' in img.info.keys():
            res_x, res_y = img.info['jfif_density']
            unit = None
            if res_x and res_y:
                if 'jfif_unit' in img.info.keys():
                    unit = img.info['jfif_unit']
                if unit == 1:
                    scale = self.unittouu('1in')
                    res_u = 'dots per inch'
                elif unit == 2:
                    scale = self.unittouu('1cm')
                    res_u = 'dots per cm'
                elif unit == 0:   # jfif_density is pixel aspect ratio
                    res_u = None  # (usually 1:1)
                else:  # unit unknown: fall back to dots per inch (?)
                    scale = self.unittouu('1in')
                    res_u = 'dots per inch'
            if res_x and res_y and res_u:
                img_res['ok'] = True
                img_res['scale'] = scale
                img_res['res_x'] = res_x
                img_res['res_y'] = res_y
                img_res['res_u'] = res_u

    def read_pil_dpi(self, img, img_res):
        """Read dpi info for image resolution via PIL imaging module."""
        showme("Trying dpi data...")
        res_x = res_y = res_u = None
        if 'dpi' in img.info.keys():
            res_x, res_y = img.info['dpi']
            if res_x and res_y:
                scale = self.unittouu('1in')
                res_u = 'dots per inch'
            if res_x and res_y and res_u:
                img_res['ok'] = True
                img_res['scale'] = scale
                img_res['res_x'] = res_x
                img_res['res_y'] = res_y
                img_res['res_u'] = res_u

    def read_wand_exif(self, img, img_res):
        """Read Exif data for image resolution via Wand imaging module."""
        showme("Trying resolution via Wand/ImageMagick Exif...")
        res_x = res_y = res_u = None
        if hasattr(img, 'metadata'):
            unit = None
            if 'exif:XResolution' in img.metadata:
                res_x = wand_exif_rational_to_float(
                    img.metadata['exif:XResolution'])
            if 'exif:YResolution' in img.metadata:
                res_y = wand_exif_rational_to_float(
                    img.metadata['exif:YResolution'])
            if 'exif:ResolutionUnit' in img.metadata:
                unit = int(img.metadata['exif:ResolutionUnit'])
            if res_x and res_y:
                if unit == 2:
                    scale = self.unittouu('1in')
                    res_u = 'dots per inch'
                elif unit == 3:
                    scale = self.unittouu('1cm')
                    res_u = 'dots per cm'
                else:  # unknown (1)
                    res_u = None
            if res_x and res_y and res_u:
                img_res['ok'] = True
                img_res['scale'] = scale
                img_res['res_x'] = res_x
                img_res['res_y'] = res_y
                img_res['res_u'] = res_u

    def read_wand_png(self, img, img_res):
        """Read PNG chunks for image resolution via Wand imaging module."""
        showme("Trying resolution via Wand/ImageMagick PNG...")
        res_x = res_y = res_u = None
        if hasattr(img, 'metadata'):
            unit = None
            if 'png:pHYs' in img.metadata:
                phys_str = img.metadata['png:pHYs']
                # TODO: pythonic solution to retrieve values from pHYs
                phys_list = phys_str.split(',')
                res_x = int(phys_list[0].split('=')[1])
                res_y = int(phys_list[1].split('=')[1])
                unit = int(phys_list[2].split('=')[1])
            if res_x and res_y:
                if unit == 1:
                    scale = self.unittouu('1m')
                    res_u = 'dots per m'
                else:  # unknown (1)
                    res_u = None
            if res_x and res_y and res_u:
                img_res['ok'] = True
                img_res['scale'] = scale
                img_res['res_x'] = res_x
                img_res['res_y'] = res_y
                img_res['res_u'] = res_u

    def read_wand(self, img, img_res):
        """Read IM data for image resolution via Wand imaging module."""
        showme("Trying resolution via Wand/ImageMagick...")
        res_x = res_y = res_u = None
        if hasattr(img, 'resolution'):
            res_x, res_y = img.resolution
            if res_x and res_y:
                # FIXME: Wand apparently truncates resolution to int
                # (--> imprecise resolution-based image size with cm)
                if img.units == 'pixelspercentimeter':
                    scale = self.unittouu('1cm')
                    res_u = 'dots per cm'
                elif img.units == 'pixelsperinch':
                    scale = self.unittouu('1in')
                    res_u = 'dots per inch'
                else:  # undefined
                    res_u = None
            if res_x > 0 and res_y > 0 and res_u:
                img_res['ok'] = True
                img_res['scale'] = scale
                img_res['res_x'] = res_x
                img_res['res_y'] = res_y
                img_res['res_u'] = res_u

    def image_resolution(self, img):
        """Retrieve available information about image resolution."""
        img_res = {
            'scale': self.unittouu('1px'),
            'res_x': 1.0,
            'res_y': 1.0,
            'res_u': None,
            'ok': False,
        }
        if modimg.USE_PIL:
            if not img_res['ok']:
                self.read_pil_exif(img, img_res)
            if not img_res['ok']:
                self.read_pil_jfif(img, img_res)
            if not img_res['ok']:
                self.read_pil_dpi(img, img_res)
        elif modimg.USE_WAND:
            if not img_res['ok']:
                self.read_wand_exif(img, img_res)
            if not img_res['ok']:
                self.read_wand_png(img, img_res)
            if not img_res['ok']:
                self.read_wand(img, img_res)
        if img_res['res_u'] is not None:
            showme('Resolution: {0} x {1} {2}\n'.format(img_res['res_x'],
                                                        img_res['res_y'],
                                                        img_res['res_u']))
        else:
            showme('Image size: {0} x {1}\n'.format(img.size[0], img.size[1]))
        return (img_res['scale'], img_res['res_x'], img_res['res_x'])


class ImageReset(modimg.ImageAttributer, ImageResolution):
    """ImageModifier-based class to (re-)set <image> dimension."""

    def __init__(self):
        """Init base class and add options for ImageReset class."""
        modimg.ImageAttributer.__init__(self)
        ImageResolution.__init__(self)

        # debug
        self.OptionParser.add_option("--scale",
                                     action="store", type="string",
                                     dest="scale", default="csspixel",
                                     help="Image scale")
        self.OptionParser.add_option("--img_info",
                                     action="store", type="inkbool",
                                     dest="img_info", default=True,
                                     help="Image info")

    def img_node_size(self, image, img_node, img_scale):
        """Return dimensions of <image> element based on resolution."""
        width, height = image.size
        # get scale/resolution info
        if img_scale == 'csspixel':
            scale = self.unittouu('1px')
            res_x = res_y = 1.0
        elif img_scale == 'svguserunits':
            scale = 1.0
            res_x = res_y = 1.0
        elif img_scale == 'resolution':
            scale, res_x, res_y = self.image_resolution(image)
        else:
            scale = self.unittouu('1in')
            try:
                res_x = res_y = float(self.options.scale)
            except ValueError:
                res_x = res_y = 96.0
        # debug info
        showme('image width:\t\t{}\n'.format(image.size[0]) +
               'img_node width:\t{}\n'.format(img_node.get('width')) +
               '{} * {} / {} = {}\n'.format(
                   width, scale, res_x, width * scale / res_x))
        showme('image height:\t\t{}\n'.format(image.size[1]) +
               'img_node height:\t{}\n'.format(img_node.get('height')) +
               '{} * {} / {} = {}\n'.format(
                   height, scale, res_y, height * scale / res_y))
        # return dimensions of SVG <image> node
        return (width * scale / res_x, height * scale / res_y)

    def modify_image(self, img_node, path=None, points=None, subs=None):
        """Modify <image> dimensions based on image data and options."""
        modimg.select_imaging_module(self.options.imaging_module)
        image = modimg.prep_image(img_node)
        if image is not None:
            # get image node size
            node_size = self.img_node_size(image, img_node, self.options.scale)
            # change width, height attributes of <image> element
            img_node.set('width', str(node_size[0]))
            img_node.set('height', str(node_size[1]))
            # report image info
            if self.options.img_info:
                modimg.image_info(image)


if __name__ == '__main__':
    ME = ImageReset()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
