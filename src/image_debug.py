#!/usr/bin/env python
"""
image_debug.py - extension to debug image-modifying extensions and
                 python imaging modules

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# local library
from image_lib import common as modimg


class ImageDebug(modimg.ImageModifier1):
    """ImageModifier-based class to test package 'image_lib'."""

    def __init__(self):
        """Init base class and add options for ImageDebug class."""
        modimg.ImageModifier1.__init__(self)
        # debug
        self.OptionParser.add_option("--insert_img",
                                     action="store", type="inkbool",
                                     dest="insert_img", default=True,
                                     help="Insert built-in image")
        self.OptionParser.add_option("--built_in",
                                     action="store", type="string",
                                     dest="built_in", default="rose:",
                                     help="Built-in demo image")
        self.OptionParser.add_option("--img_info",
                                     action="store", type="inkbool",
                                     dest="img_info", default=True,
                                     help="Image info")
        self.OptionParser.add_option("--wrap",
                                     action="store", type="string",
                                     dest="wrap", default="no",
                                     help="Wrap result")

    def insert_stock_image(self):
        """Insert stock image if supported by imaging module.

        Display image info of generated image if requested.
        """
        if modimg.USE_WAND:
            with modimg.ImageWand(filename=self.options.built_in) as img:
                img.alpha_channel = True
                # create new image node
                img_node = modimg.create_img_node()
                self.current_layer.append(img_node)
                # scale image based on drawing scale
                scale_x = scale_y = self.unittouu('1px')
                img_node.set('width', str(img.width * scale_x))
                img_node.set('height', str(img.height * scale_y))
                if self.options.img_info:
                    modimg.image_info(img)
                # save and close image
                modimg.save_image(img_node, img, img_format="keep")
        elif modimg.USE_PIL:
            pass
        else:
            raise RuntimeError(modimg.NO_MODULE)

    def debug_image(self):
        """Display image info of last selected <image> element."""
        if modimg.DEBUG:
            modimg.report_imaging_module("debug_image()")
        img_node = None
        # check selected objects for image
        for node in self.selected.values():
            if modimg.is_image(node):
                img_node = node
        if img_node is not None:
            # image found, display info if requested
            img = modimg.prep_image(img_node)
            if self.options.img_info:
                modimg.image_info(img)
            img.close()
        else:
            # no image found, add stock image if requested
            if self.options.insert_img:
                self.insert_stock_image()

    def modify_image(self, img_node, path, points=None, subs=None):
        """Report default, selected Python Imaging module, run test."""
        if modimg.DEBUG:
            modimg.report_imaging_module("modify_image()")
        modimg.select_imaging_module(self.options.imaging_module)
        self.debug_image()


if __name__ == '__main__':
    ME = ImageDebug()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
