#!/usr/bin/env python
"""
image_freeform.py  - freeform (Shepards) distortion of a bitmap image
                     based on 2 helper paths (stack order determines
                     initial source/dest roles)

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# standard library
from math import ceil

# local library
import inkex
from image_lib import common as modimg


class ImageFreeform(modimg.ImageModifier2):
    """ImageModifier-based class to apply Shepards distortion to bitmap."""

    def __init__(self):
        """Init base class and add options for ImageFreeform class."""
        modimg.ImageModifier2.__init__(self)
        # shepards
        self.OptionParser.add_option("--mode",
                                     action="store", type="string",
                                     dest="mode", default="shepards",
                                     help="Distortion method")
        self.OptionParser.add_option("--reverse_subpaths",
                                     action="store", type="inkbool",
                                     dest="reverse_subpaths", default=False,
                                     help="Reverse direction of helper paths")
        self.OptionParser.add_option("--reverse_source",
                                     action="store", type="inkbool",
                                     dest="reverse_source", default=False,
                                     help="Reverse direction of source")
        self.OptionParser.add_option("--reverse_dest",
                                     action="store", type="inkbool",
                                     dest="reverse_dest", default=False,
                                     help="Reverse direction of destination")
        self.OptionParser.add_option("--pin_edges",
                                     action="store", type="inkbool",
                                     dest="pin_edges", default=False,
                                     help="Pin edges of source area")
        self.OptionParser.add_option("--edge_divisions",
                                     action="store", type="int",
                                     dest="edge_divisions", default=10,
                                     help="Edge division factor of source")
        self.OptionParser.add_option("--wrap",
                                     action="store", type="string",
                                     dest="wrap", default="no",
                                     help="Wrap result")

    def get_pinpoints(self, (width, height)):
        """Return list of evenly-spaced points along the image edges"""
        min_div = self.options.edge_divisions
        min_spacing = min(width, height) / min_div
        # width pins
        w_div = int(ceil(width / min_spacing))
        w_spacing = width / w_div
        w_pins = [(i * w_spacing, j)
                  for j in [0, height]
                  for i in range(w_div)]
        # height pins
        h_div = int(ceil(height / min_spacing))
        h_spacing = height / h_div
        h_pins = [(j, i * h_spacing)
                  for j in [0, width]
                  for i in range(h_div)]
        return w_pins + h_pins

    def modify_image(self, img_node, path, points=None, subs=2):
        """Modify image content based on helper path and options."""
        if modimg.USE_PIL:
            inkex.errormsg(self.not_with_pil)
            image = dest = None
        else:
            image, dest = modimg.check_req(img_node, path, points, subs)
        if image is not None and dest is not None:
            # compensate transforms for dest
            mat_p2i = modimg.transform_path_to_image(path, img_node, image)
            modimg.mat.apply_to(mat_p2i, dest)

            # prepare source, dest for pointlist
            if self.options.reverse_subpaths:
                dest.reverse()
            source = modimg.csp_to_points(dest, 0, points)
            dest = modimg.csp_to_points(dest, 1, points)

            # start and direction of source helper path
            if self.options.reverse_source:
                source.reverse()

            # start and direction of destination helper path
            if self.options.reverse_dest:
                dest.reverse()

            # pin edges of source area
            if self.options.pin_edges:
                pinpoints = self.get_pinpoints(image.size)
                source += pinpoints
                dest += pinpoints
            points = len(source)

            # transformation method
            method = self.options.mode

            pointlist = [p[i][j]
                         for i in range(points)
                         for p in [source, dest]
                         for j in range(2)]
            image.virtual_pixel = self.options.virtual_pixel
            image.matte_color = modimg.ColorWand(self.options.matte_color)
            image.distort(method, pointlist)
            modimg.save_image(img_node, image)

            # post-processing
            path.getparent().remove(path)


if __name__ == '__main__':
    ME = ImageFreeform()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
