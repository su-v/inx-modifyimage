#!/usr/bin/env python
"""
image_metadata.py - extension to extract image metadata (Exif, IPTC, XMP)
                    to text elements inserted below the bitmap image

Copyright (C) 2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# local library
import inkex
from image_lib import common as modimg
from image_lib import metadata as md
from simplestyle import formatStyle


# helper functions

def split_csv_string(string):
    """Split string with comma-separated values into list."""
    alist = []
    if string:
        alist = [c.strip() for c in string.split(',') if c]
    return alist


def concat(value, string):
    """Concatenate value and string for CSS property."""
    return '{0}{1}'.format(str(value), string)


def format_tag_line(tag, value):
    """Concatenate tag and value to single line of text."""
    return '{0}: {1}'.format(tag, value)


def add_lines(tag_info, image_tags, user_tags, lines):
    """Append output tags to list of line strings."""

    def set_output(keyname, keyvalue):
        """Add dict entry, optionally strip tagname for output."""
        # NOTE: stripping early affects output sort order
        # if tag_info['strip']:
        #     keyname = md.strip_tag_prefix(tag_info['type'], keyname)
        output_tags[keyname] = str(keyvalue)

    output_tags = {}
    for key, value in image_tags.items():
        if tag_info['scope'] == 'print_only':
            for tag in user_tags:
                if md.match_keys(tag_info['type'], key, tag):
                    set_output(key, value)
                    break
        elif tag_info['scope'] == 'print_without':
            exclude_key = False
            for tag in user_tags:
                if md.match_keys(tag_info['type'], key, tag):
                    exclude_key = True
                    break
            if not exclude_key:
                set_output(key, value)
        else:  # tag_info['scope'] == 'print_all'
            set_output(key, value)
    # format dict into lines of text
    for tag in sorted(output_tags.keys()):
        # NOTE: strip after sorting for consistent order
        if tag_info['strip']:
            tagname = md.strip_tag_prefix(tag_info['type'], tag)
        else:
            tagname = tag
        lines.append(format_tag_line(tagname, output_tags[tag]))


def notify_user(linecount, tag_type):
    """Notify user if no tags of specified type are in image."""
    if not linecount:
        inkex.debug('No {0} metadata found in image.'.format(tag_type))


def render_lines(lines, params):
    """Create multi-line text element with a tspan for each tag line."""
    text = inkex.etree.Element(inkex.addNS('text', 'svg'))
    text.set('x', str(params['x']))
    text.set('y', str(params['y']))
    text.set('style', params['style'])
    md.showme("\nLines:")
    for i, line in enumerate(lines):
        md.showme('{}: {}'.format(i, line))
        tspan = inkex.etree.SubElement(text, inkex.addNS('tspan', 'svg'))
        tspan.text = line
        tspan.set('x', str(params['x']))
        tspan.set('y', str(params['y'] + i * params['line-height']))
        tspan.set(inkex.addNS('role', 'sodipodi'), "line")
    return text


class ImageTags(modimg.ImageMassModifier):
    """ImageModifier-based class to render metadata tags per image."""

    def __init__(self):
        """Init base class and add options for ImageTags class."""
        modimg.ImageMassModifier.__init__(self)

        # Common options
        self.OptionParser.add_option("--font_family",
                                     action="store", type="string",
                                     dest="font_family", default="monospace",
                                     help="Font family for tag text")
        self.OptionParser.add_option("--font_size",
                                     action="store", type="float",
                                     dest="font_size", default=12,
                                     help="Font size for tag text")
        self.OptionParser.add_option("--font_size_unit",
                                     action="store", type="string",
                                     dest="font_size_unit", default="pt",
                                     help="Font size unit for tag text")
        # Exif options
        self.OptionParser.add_option("--exif_scope",
                                     action="store", type="string",
                                     dest="exif_scope",
                                     default="exif_print_all",
                                     help="Print all Exif metadata info")
        self.OptionParser.add_option("--exif_tags",
                                     action="store", type="string",
                                     dest="exif_tags",
                                     default="DateTime,Orientation",
                                     help="Exif tags to be printed")
        self.OptionParser.add_option("--exif_strip",
                                     action="store", type="inkbool",
                                     dest="exif_strip",
                                     default=False,
                                     help="Strip Exif prefix")
        # IPTC options
        self.OptionParser.add_option("--iptc_scope",
                                     action="store", type="string",
                                     dest="iptc_scope",
                                     default="iptc_print_all",
                                     help="Print all IPTC metadata info")
        self.OptionParser.add_option("--iptc_tags",
                                     action="store", type="string",
                                     dest="iptc_tags",
                                     default="",
                                     help="IPTC tags to be printed")
        self.OptionParser.add_option("--iptc_strip",
                                     action="store", type="inkbool",
                                     dest="iptc_strip",
                                     default=False,
                                     help="Strip IPTC prefix")
        # XMP options
        self.OptionParser.add_option("--xmp_scope",
                                     action="store", type="string",
                                     dest="xmp_scope",
                                     default="xmp_print_all",
                                     help="Print all XMP metadata info")
        self.OptionParser.add_option("--xmp_tags",
                                     action="store", type="string",
                                     dest="xmp_tags",
                                     default="",
                                     help="XMP tags to be printed")
        self.OptionParser.add_option("--xmp_strip",
                                     action="store", type="inkbool",
                                     dest="xmp_strip",
                                     default=False,
                                     help="Strip XMP prefix")
        # backend
        self.OptionParser.add_option("--prefer_gexiv2",
                                     action="store", type="inkbool",
                                     dest="prefer_gexiv2", default=True,
                                     help="Prefer GExiv2")

    def text_params(self, img_node, image):
        """Set position, style and other attributes for output text."""
        # predefined constants
        unit_uu = 'px'
        linespacing = 1.25
        # get user options
        font_family = self.options.font_family.strip()
        font_size = self.options.font_size
        font_size_unit = self.options.font_size_unit
        # calculate line height, position
        font_size_uu = self.unittouu(concat(font_size, font_size_unit))
        line_height = font_size_uu * linespacing
        text_x = float(img_node.get('x', 0))
        text_y = (float(img_node.get('y', 0)) +
                  float(img_node.get('height', image.size[1])) +
                  line_height)
        # define text style
        text_style = {}
        text_style['font-family'] = font_family
        text_style['font-size'] = concat(font_size_uu, unit_uu)
        text_style['line-height'] = '{0:.0%}'.format(linespacing)
        # return dict with text paramaters
        return {
            'x': text_x,
            'y': text_y,
            'style': formatStyle(text_style),
            'line-height': line_height,
        }

    def add_exif_lines(self, img_node, lines):
        """Append output Exif tags to list of line strings."""
        image_tags = md.read_exif(img_node)
        user_tags = split_csv_string(self.options.exif_tags)
        tag_info = {
            'type': 'exif',
            'scope': self.options.exif_scope,
            'strip': self.options.exif_strip,
        }
        add_lines(tag_info, image_tags, user_tags, lines)

    def add_iptc_lines(self, img_node, lines):
        """Append output IPTC tags to list of line strings."""
        image_tags = md.read_iptc(img_node)
        user_tags = split_csv_string(self.options.iptc_tags)
        tag_info = {
            'type': 'iptc',
            'scope': self.options.iptc_scope,
            'strip': self.options.iptc_strip,
        }
        add_lines(tag_info, image_tags, user_tags, lines)

    def add_xmp_lines(self, img_node, lines):
        """Append output XMP tags to list of line strings."""
        image_tags = md.read_xmp(img_node)
        user_tags = split_csv_string(self.options.xmp_tags)
        tag_info = {
            'type': 'xmp',
            'scope': self.options.xmp_scope,
            'strip': self.options.xmp_strip,
        }
        add_lines(tag_info, image_tags, user_tags, lines)

    def process_image(self, img_node, image):
        """Process <image> based on image data and options."""
        md.check_gexiv2(self.options.prefer_gexiv2)
        # output lines
        lines = []
        # get user option based on current tab
        if self.options.tab == '"nb_exif"':
            self.add_exif_lines(img_node, lines)
            notify_user(len(lines), 'Exif')
        elif self.options.tab == '"nb_iptc"':
            self.add_iptc_lines(img_node, lines)
            notify_user(len(lines), 'IPTC')
        elif self.options.tab == '"nb_xmp"':
            self.add_xmp_lines(img_node, lines)
            notify_user(len(lines), 'XMP')
        elif self.options.tab == '"help"':
            pass
        else:
            inkex.debug("Unknown tab - nothing to do.")
        if len(lines):
            # render tag/value pairs as text
            params = self.text_params(img_node, image)
            text = render_lines(lines, params)

            # insert text element after img_node
            index = img_node.getparent().index(img_node)
            img_node.getparent().insert(index+1, text)
            modimg.mat.apply_copy_from(img_node, text)

    def modify_image(self, img_node, path=None, points=None, subs=None):
        """Modify <image> based on image data and options."""
        modimg.select_imaging_module(self.options.imaging_module)
        image = modimg.prep_image(img_node)
        if image is not None:
            self.process_image(img_node, image)


if __name__ == '__main__':
    ME = ImageTags()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
