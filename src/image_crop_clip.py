#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
image_crop_clip.py - Crop clipped bitmap image with bbox of clipPath

Copyright (C) 2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Todo:
    - Support for clipPathUnits="objectBoundingBox"
    - Investigate cases where clip-rule matters for crop box

Not supported:
    - Complex (e.g. nested) clipPath definitions
    - Multiple clip-paths on parent containers

"""
# standard library
from math import ceil
from copy import deepcopy

# local library
from image_lib import common as modimg


class ImageCropClip(modimg.ImageMassModifier):
    """ImageModifier-based class to crop bitmap image."""

    def __init__(self):
        """Init base class and add options for ImageCrop class."""
        modimg.ImageMassModifier.__init__(self)
        # crop
        self.OptionParser.add_option("--crop_to_ancestor_clip",
                                     action="store",
                                     type="inkbool",
                                     dest="crop_to_ancestor_clip",
                                     default=False,
                                     help="Crop to ancestor clips")
        self.OptionParser.add_option("--wrap",
                                     action="store",
                                     type="string",
                                     dest="wrap",
                                     default="no",
                                     help="Wrap result")

    def get_params(self):
        """Return parameter values valid for all image changes."""
        return None

    def modify_image(self, img_node, params, points=None, subs=None):
        """Modify image content based on helper path."""
        # pylint: disable=too-many-locals
        # pylint: disable=unused-argument
        modimg.select_imaging_module(self.options.imaging_module)
        image = modimg.prep_image(img_node)
        check_parents = self.options.crop_to_ancestor_clip
        path, dest = self.get_clip_geom(img_node, check_parents)
        if image is not None and dest is not None:
            # original image scale
            scale_x, scale_y = modimg.get_image_scale(image, img_node)

            # compensate transforms for dest
            mat_p2i = modimg.transform_path_to_image(path, img_node, image)
            cropbox_dest = deepcopy(dest)
            modimg.mat.apply_to(mat_p2i, dest)

            # prepare dest for crop points
            dest = modimg.csp_to_points(dest, 0, points)

            # dest bbox in image coords (don't exceed image area)
            x_coords, y_coords = zip(*dest)
            x_min = max(min(*x_coords), 0)
            y_min = max(min(*y_coords), 0)
            x_max = min(max(*x_coords), image.size[0])
            y_max = min(max(*y_coords), image.size[1])

            # pointlist from bbox of dest
            pointlist = [int(i) for i in [x_min, y_min,
                                          ceil(x_max), ceil(y_max)]]
            if modimg.USE_WAND:
                img_format = "keep"
                image.crop(*pointlist)
            elif modimg.USE_PIL:
                img_format = image.format
                image = image.crop(pointlist)
            modimg.save_image(img_node, image, img_format)

            # adjust position and size of modified source image,
            # compensate image scale
            img_node.set('width', str(image.size[0] / scale_x))
            img_node.set('height', str(image.size[1] / scale_y))
            img_node.set('x', str(float(img_node.get("x", "0")) +
                                  int(x_min) / scale_x))
            img_node.set('y', str(float(img_node.get("y", "0")) +
                                  int(y_min) / scale_y))

            # post-processing
            if self.options.wrap.startswith('clip_release'):
                keep = False if self.options.wrap.endswith('delete') else True
                self.clip_release(path, keep)
            elif self.options.wrap.startswith('crop_box'):
                cropbox = modimg.draw_cropbox(path, cropbox_dest)
                if self.options.wrap.endswith('group'):
                    self.wrap_result(img_node, cropbox, "group")
            else:
                self.wrap_result(img_node, path, self.options.wrap)


if __name__ == '__main__':
    ME = ImageCropClip()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
