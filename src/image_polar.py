#!/usr/bin/env python
"""
image_polar.py - polar distortion of a bitmap image based on
                 parameters taken from a 3-node helper path or
                 from numeric input

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


From ImageMagick documentation:

    Polar Distortion (full circle distorts)

    The 'Polar' distort (Added IM v6.4.2-6) is a more low level
    version of the 'Arc' distortion above. But it will not
    automatically do 'bestfit', nor does it try to preserve the
    aspect ratios of images.

    The 6 optional floating point arguments are...

        Radius_Max
        Radius_Min
        Center_X,Center_Y
        Start_Angle,End_Angle

    All the arguments are optional at the spaced positions.

.. _Source:
    http://www.imagemagick.org/Usage/distorts/#polar

"""
# standard library
import math

# local library
import inkex
from image_lib import common as modimg
from image_lib.geom import Point
from image_lib.geom import Polar


def showme(msg):
    """Print debug output only if global DEBUG variable is set."""
    if modimg.DEBUG:
        inkex.debug(msg)


def draw_helper_path(pointlist):
    """Construct helper path in image cooords from pointlist."""
    # pylint: disable=unused-variable
    # pylint: disable=too-many-locals
    if len(pointlist) == 6 and pointlist[0] > 0:
        r_max, r_min, center_x, center_y, t_start, t_end = pointlist
        center = Point(center_x, center_y)
        t_start, t_end = [math.radians(90.0 - t) for t in (t_start, t_end)]
        point1, point4 = [(center + Polar(r, t_start)) for r in (r_max, r_min)]
        point2, point3 = [(center + Polar(r, t_end)) for r in (r_max, r_min)]
        path = inkex.etree.Element(inkex.addNS('path', 'svg'))
        path_d = point1.format_move()
        path_d += center.format_line()
        path_d += point3.format_line()
        path.set('d', path_d)
        path_style = "stroke:red;stroke-width:2px;fill:none;fill-opacity:0.25"
        path.set('style', path_style)
        return path


def draw_clip_path(pointlist):
    """Construct clip-path in image coords from pointlist.

    The data in pointlist is the as used for the IM polar distortion.
    """
    # pylint: disable=too-many-locals
    if len(pointlist) == 6 and pointlist[0] > 0:
        r_max, r_min, center_x, center_y, t_start, t_end = pointlist
        center = Point(center_x, center_y)
        sweep = t_start - t_end
        # convert from degree in IM image coords to radians
        t_start, t_end = [math.radians(90.0 - t) for t in (t_start, t_end)]
        # path start and end nodes
        point1, point4 = [(center + Polar(r, t_start)) for r in (r_max, r_min)]
        # draw path
        path = inkex.etree.Element(inkex.addNS('path', 'svg'))
        path_d = point1.format_move()   # start outer arc
        if abs(sweep) >= 360:
            # clip-path is a full (continuous) circle / annulus
            large_arc = 1
            sweep_max = 0
            sweep_min = 1
            # calculate mid-points to draw 2 semi-circles
            point2, point3 = [(center + Polar(r, t_start + math.pi))
                              for r in (r_max, r_min)]
            # path data
            path_d += point2.format_arc(r_max, large_arc, sweep_max)
            path_d += point1.format_arc(r_max, large_arc, sweep_max)
            path_d += Point.format_close()
            path_d += point4.format_move()  # start inner arc
            path_d += point3.format_arc(r_min, large_arc, sweep_min)
            path_d += point4.format_arc(r_min, large_arc, sweep_min)
        else:
            # clip-path is a sector of the circle / annulus
            large_arc = int(abs(sweep) / 180) % 2
            sweep_max = 0 if sweep <= 0 else 1
            sweep_min = 1 if sweep <= 0 else 0
            # calculate arc end points
            point2, point3 = [(center + Polar(r, t_end))
                              for r in (r_max, r_min)]
            # path data
            path_d += point2.format_arc(r_max, large_arc, sweep_max)
            path_d += point3.format_line()  # start inner arc
            path_d += point4.format_arc(r_min, large_arc, sweep_min)
        path_d += Point.format_close()
        path.set('d', path_d)
        path_style = "stroke:red;stroke-width:2px;fill:blue;fill-opacity:0.25"
        path.set('style', path_style)
        return path


class ImagePolar(modimg.ImageModifier1):
    """ImageModifier-based class to apply Polar distortion to bitmap."""

    def __init__(self):
        """Init base class and add options for ImagePolar class."""
        modimg.ImageModifier1.__init__(self)
        # geometry from path
        self.OptionParser.add_option("--rotate_180",
                                     action="store", type="inkbool",
                                     dest="rotate_180", default=False,
                                     help="Rotate 180 degree (bool)")
        # geometry custom input
        self.OptionParser.add_option("--radius_max",
                                     action="store", type="int",
                                     dest="radius_max", default=100,
                                     help="Radius_max")
        self.OptionParser.add_option("--radius_min",
                                     action="store", type="int",
                                     dest="radius_min", default=0,
                                     help="Radius_min")
        self.OptionParser.add_option("--center_x",
                                     action="store", type="int",
                                     dest="center_x", default=50,
                                     help="Center_X")
        self.OptionParser.add_option("--center_y",
                                     action="store", type="int",
                                     dest="center_y", default=50,
                                     help="Center_Y")
        self.OptionParser.add_option("--angle_start",
                                     action="store", type="int",
                                     dest="angle_start", default=-180,
                                     help="Start_Angle")
        self.OptionParser.add_option("--angle_end",
                                     action="store", type="int",
                                     dest="angle_end", default=180,
                                     help="End_Angle")
        self.OptionParser.add_option("--rotate",
                                     action="store", type="int",
                                     dest="rotate", default=0,
                                     help="Rotate image (degrees)")
        self.OptionParser.add_option("--draw_helper_path",
                                     action="store", type="inkbool",
                                     dest="draw_helper_path", default=False,
                                     help="Draw helper path")
        # common options
        self.OptionParser.add_option("--bestfit",
                                     action="store", type="inkbool",
                                     dest="bestfit", default=True,
                                     help="Best-fit result")
        self.OptionParser.add_option("--wrap",
                                     action="store", type="string",
                                     dest="wrap", default="no",
                                     help="Wrap result")
        # tabs
        self.OptionParser.add_option("--nb_geom",
                                     action="store", type="string",
                                     dest="nb_geom",
                                     help="Current tab for geometry input")

    def polar_geometry(self, image, dest):
        """Calculate geometric parameters for polar distortion (IM)."""
        if self.options.nb_geom == '"path"':
            # use geometry from dest path
            start, center, end = [Point(*p) for p in dest]
            radius_max, radius_min = center.distances(start, end)

            if radius_max <= radius_min:
                inkex.debug("Radius_max is smaller than Radius_min - " +
                            "reversing point list.")
                end, center, start = [Point(*p) for p in dest]
                radius_max, radius_min = center.distances(start, end)

            start_end = [(90.0 - math.degrees(t))
                         for t in center.angles(start, end)]

            # debug
            showme('Start angle: {}'.format(start_end[0]))
            showme('End angle: {}'.format(start_end[1]))
            showme('Sweep angle: {}'.format(math.degrees(center.sweep(start,
                                                                      end))))

            # optionall rotate image before polar distortion
            if self.options.rotate_180:
                image.rotate(180.0, reset_coords=True)

        elif self.options.nb_geom == '"custom"':
            # use geometry from input parameters
            if self.options.radius_max == -1:
                # "If you use a special 'Radius_Max' value of exactly
                # '-1' the radius of the distorted image is set to the
                # distance from the center to the furthest corner
                # (diagonal). This is to provide an ideal 'reverse' for
                # a full image 'DePolar' distortion (...)."
                # Source: http://www.imagemagick.org/Usage/distorts/#polar
                center = Point(
                    self.options.center_x/100.0 * image.width,
                    self.options.center_y/100.0 * image.height
                )
                radius_max = max(
                    center.distance(Point(0, 0)),
                    center.distance(Point(image.width, 0)),
                    center.distance(Point(image.width, image.height)),
                    center.distance(Point(0, image.height))
                )
                radius_min = self.options.radius_min
            elif self.options.radius_max == 0:
                # "As the 'Radius_Max' must be given, it should some
                # positive value. However if you give a value of '0' it
                # will be set to the distance between the center and the
                # closest edge, so that if the other values are not given
                # (defaults), the whole input image is mapped into a
                # circle in the middle of the image."
                # Source: http://www.imagemagick.org/Usage/distorts/#polar
                center = Point(
                    min(abs(self.options.center_x/100.0), 1) * image.width,
                    min(abs(self.options.center_y/100.0), 1) * image.height
                )
                radius_max = min(
                    abs(center.x),
                    abs(image.width - center.x),
                    abs(center.y),
                    abs(image.height - center.y)
                )
                radius_max = radius_max or min(image.width, image.height)
                radius_min = self.options.radius_min
            else:
                center = Point(
                    self.options.center_x/100.0 * image.width,
                    self.options.center_y/100.0 * image.height
                )
                if self.options.radius_max > self.options.radius_min:
                    radius_max = self.options.radius_max/100.0 * image.height
                    radius_min = self.options.radius_min/100.0 * image.height
                else:
                    inkex.debug("Radius_max is smaller than Radius_min - " +
                                "swapping values.")
                    radius_max = self.options.radius_min/100.0 * image.height
                    radius_min = self.options.radius_max/100.0 * image.height

            start_end = [self.options.angle_start, self.options.angle_end]

            # optionall rotate image before polar distortion
            if self.options.rotate != 0:
                image.rotate(float(self.options.rotate), reset_coords=True)

        # IM arguments:
        # Radius_Max Radius_Min Center_X,Center_Y Start_Angle,End_Angle
        return [radius_max], [radius_min], center.cartesian, start_end

    def modify_image(self, img_node, path, points=3, subs=1):
        """Modify image content based on helper path and options."""
        # pylint: disable=too-many-locals
        if modimg.USE_PIL:
            inkex.errormsg(self.not_with_pil)
            image = dest = None
        else:
            image, dest = modimg.check_req(img_node, path, points, subs)
        if image is not None and dest is not None:
            # original image scale
            scale_x, scale_y = modimg.get_image_scale(image, img_node)

            # compensate transforms for dest
            mat_p2i = modimg.transform_path_to_image(path, img_node, image)
            mat_i2n = modimg.mat_image_to_img_node(image, img_node)
            modimg.mat.apply_to(mat_p2i, dest)

            # prepare dest for pointlist
            dest = modimg.csp_to_points(dest, 0, points)

            # transformation method
            method = 'polar'  # self.options.mode

            # merge parameters into list:
            pointlist = []
            for param in self.polar_geometry(image, dest):
                pointlist += param

            image.virtual_pixel = self.options.virtual_pixel
            image.matte_color = modimg.ColorWand(self.options.matte_color)
            image.distort(method, pointlist, self.options.bestfit)
            image.reset_coords()
            modimg.save_image(img_node, image)

            # adjust position and size of modified source image,
            # compensate image scale
            if self.options.bestfit:
                img_node.set('width', str(image.width / scale_x))
                img_node.set('height', str(image.height / scale_y))
                img_node.set('x', str(float(img_node.get('x')) +
                                      ((pointlist[2] - 0.5*image.width) /
                                       scale_x)))
                img_node.set('y', str(float(img_node.get('y')) +
                                      ((pointlist[3] - 0.5*image.height) /
                                       scale_y)))

            # optionally draw helper path for custom input
            if (self.options.nb_geom == '"custom"' and
                    self.options.draw_helper_path):
                hpath = draw_helper_path(pointlist)
                img_node.getparent().append(hpath)
                modimg.mat.apply_to_d(mat_i2n, hpath)

            # post-processing
            if self.options.wrap == 'clip':
                cpath = draw_clip_path(pointlist)
                if cpath is not None:
                    img_node.getparent().append(cpath)
                    modimg.mat.apply_to_d(mat_i2n, cpath)
                    if self.options.nb_geom == '"path"':
                        path.getparent().remove(path)
                    if not modimg.DEBUG:
                        self.wrap_result(img_node, cpath, 'clip')
            else:
                self.wrap_result(img_node, path, self.options.wrap)


if __name__ == '__main__':
    ME = ImagePolar()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
