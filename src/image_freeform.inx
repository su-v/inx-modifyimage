<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
  <_name>Shepards Distortion</_name>
  <id>su-v/org.inkscape.effect.image_freeform</id>

  <dependency type="executable" location="extensions">image_freeform.py</dependency>
  <dependency type="executable" location="extensions">image_lib/common.py</dependency>
  <dependency type="executable" location="extensions">image_lib/transform.py</dependency>
  <dependency type="executable" location="extensions">inkex.py</dependency>
  <dependency type="executable" location="extensions">cubicsuperpath.py</dependency>
  <dependency type="executable" location="extensions">simpletransform.py</dependency>

  <param name="tab" type="notebook">
    <page name="options" _gui-text="Options">
      <param name="mode" type="enum" _gui-text="Distortion method:">
        <_item value="shepards">Shepards</_item>
      </param>
      <param name="reverse_subpaths" type="boolean" _gui-text="Reverse order of helper paths">false</param>
      <param name="reverse_source" type="boolean" _gui-text="Reverse direction of source path">false</param>
      <param name="reverse_dest" type="boolean" _gui-text="Reverse direction of destination path">false</param>
      <param name="pin_edges" type="boolean" _gui-text="Pin points along the edge of source area">true</param>
      <param name="edge_divisions" type="int" min="0" max="1000" _gui-text="Edge division factor:" _gui-description="Minimal division factor when pinning fixed control points along the edge of source area">10</param>
      <param name="virtual_pixel" type="enum" _gui-text="Virtual Pixel:">
        <_item value="transparent">Transparent</_item>
        <_item value="black">Black</_item>
        <_item value="gray">Gray</_item>
        <_item value="white">White</_item>
        <_item value="background">Background</_item>
        <_item value="constant">Constant</_item>
        <_item value="dither">Dither</_item>
        <_item value="edge">Edge</_item>
        <_item value="mirror">Mirror</_item>
        <_item value="random">Random</_item>
        <_item value="tile">Tile</_item>
        <_item value="mask">Mask</_item>
        <_item value="horizontal_tile">HorizontalTile</_item>
        <_item value="vertical_tile">VerticalTile</_item>
        <_item value="horizontal_tile_edge">HorizontalTileEdge</_item>
        <_item value="vertical_tile_edge">VerticalTileEdge</_item>
        <_item value="checker_tile">CheckerTile</_item>
        <_item value="undefined">Undefined</_item>
      </param>
      <param name="matte_color" type="enum" _gui-text="Matte Color:">
        <_item value="transparent">Transparent</_item>
        <_item value="black">Black</_item>
        <_item value="gray">Gray</_item>
        <_item value="white">White</_item>
        <_item value="red">Red</_item>
        <_item value="green">Green</_item>
        <_item value="blue">Blue</_item>
      </param>
      <param name="wrap" type="enum" gui-text="Wrap result:">
        <_item value="no">No</_item>
        <_item value="delete">Delete helper path</_item>
      </param>
    </page>
    <page name="help" _gui-text="Help">
      <param name="distort_shepards_head" type="description" appearance="header">Shepards Distortion</param>
      <param name="distort_shepards_desc" type="description" _gui-description="Distortion that moves points in terms of a Inverse Squared Distance interpolation.  The Shepards Distortion limits its distortions to areas marked by the movements, or non-movements of the given points. Its distortions are localized and restricted according to the distances between neighboring control-points, though all points still do have an averaged global effect. It is point driven, not line or area driven, so parts between the points can bulge, or swirl unexpectedly when control points, that move differently, are positioned too close together.">Inverse Squared Distance interpolation.</param>
      <param name="distort_shepards_ref_desc" type="string" _gui-text="See also">http://www.imagemagick.org/Usage/distorts/#shepards</param>
      <param name="distort_shepards_usage_head" type="description" appearance="header">Usage</param>
      <param name="distort_shepards_usage_desc" type="description" _gui-description="Source and destination control points are the corresponding nodes of two selected helper paths. The two helper paths need to have the same number of nodes.">See tooltip.</param>
    </page>
  </param>

  <effect needs-document="true" needs-live-preview="true">
    <menu-tip>Applies Freeform (Shepards) transformation to a bitmap image based on 2 helper paths.</menu-tip>
    <object-type>all</object-type>
    <effects-menu>
      <submenu _name="Modify Image"/>
    </effects-menu>
  </effect>

  <script>
    <command reldir="extensions" interpreter="python">image_freeform.py</command>
  </script>

</inkscape-extension>
