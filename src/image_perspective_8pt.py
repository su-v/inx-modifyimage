#!/usr/bin/env python
"""
image_perspective_8pt.py - perspectively transform a bitmap image based on 2
                           quadrilateral helper path

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""
# pylint: disable=too-many-locals
# pylint: disable=too-many-branches

# local library
import inkex
import cubicsuperpath
from image_lib import common as modimg


class ImagePerspective(modimg.ImageModifier2):
    """ImageModifier-based class to apply Perspective/Envelope."""

    def __init__(self):
        """Init base class and add options for ImagePerspective class."""
        modimg.ImageModifier2.__init__(self)
        # 4-point transformation
        self.OptionParser.add_option("--mode",
                                     action="store", type="string",
                                     dest="mode", default="perspective",
                                     help="Distortion method")
        self.OptionParser.add_option("--orig_source",
                                     action="store", type="string",
                                     dest="orig_source", default="1",
                                     help="Origin (top-left) of source")
        self.OptionParser.add_option("--reverse_source",
                                     action="store", type="inkbool",
                                     dest="reverse_source", default=False,
                                     help="Reverse direction of source")
        self.OptionParser.add_option("--orig_dest",
                                     action="store", type="string",
                                     dest="orig_dest", default="1",
                                     help="Origin (top-left) of destination")
        self.OptionParser.add_option("--reverse_dest",
                                     action="store", type="inkbool",
                                     dest="reverse_dest", default=False,
                                     help="Reverse direction of destination")
        self.OptionParser.add_option("--reverse_subpaths",
                                     action="store", type="inkbool",
                                     dest="reverse_subpaths", default=False,
                                     help="Reverse order of subpaths")
        self.OptionParser.add_option("--wrap",
                                     action="store", type="string",
                                     dest="wrap", default="no",
                                     help="Wrap result")

    def modify_image(self, img_node, path, points=4, subs=2):
        """Modify image content based on helper path and options."""
        modimg.select_imaging_module(self.options.imaging_module)
        image, dest = modimg.check_req(img_node, path,
                                       points, subs, alpha=True)
        if image is not None and dest is not None:
            # image dimensions
            width, height = image.size

            # compensate transforms for dest
            mat_p2i = modimg.transform_path_to_image(path, img_node, image)
            modimg.mat.apply_to(mat_p2i, dest)

            # prepare source, dest for pointlist
            if self.options.reverse_subpaths:
                dest.reverse()
            source = modimg.csp_to_points(dest, 0, points)
            dest = modimg.csp_to_points(dest, 1, points)

            # start and direction of source quadrilateral
            if self.options.reverse_source:
                source.reverse()
            for _ in range(int(self.options.orig_source) - 1):
                source.append(source.pop(0))

            # start and direction of destination quadrilateral
            if self.options.reverse_dest:
                dest.reverse()
            for _ in range(int(self.options.orig_dest) - 1):
                dest.append(dest.pop(0))

            # distort image
            if modimg.USE_PIL:
                if modimg.HAVE_NUMPY:
                    coeffs = modimg.find_perspective_coeffs(source, dest)
                    image = image.transform((width, height),
                                            modimg.ImagePIL.PERSPECTIVE,
                                            coeffs,
                                            modimg.ImagePIL.BICUBIC)
                else:
                    inkex.errormsg(self.requires_numpy)
            elif modimg.USE_WAND:
                method = self.options.mode
                ptlist = [p[i][j]
                          for i in range(points)
                          for p in [source, dest]
                          for j in range(2)]
                image.virtual_pixel = self.options.virtual_pixel
                image.matte_color = modimg.ColorWand(self.options.matte_color)
                image.distort(method, ptlist)

            # save distorted image
            modimg.save_image(img_node, image)

            # post-processing
            if self.options.wrap != 'clip':
                path.getparent().remove(path)
            else:
                path_csp = cubicsuperpath.parsePath(path.get('d'))
                if self.options.reverse_subpaths:
                    path_csp.reverse()
                path.set('d', cubicsuperpath.formatPath([path_csp[1]]))
                self.wrap_result(img_node, path, 'clip')


if __name__ == '__main__':
    ME = ImagePerspective()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
