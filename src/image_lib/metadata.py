#!/usr/bin/env python
"""
image_lib.metadata  - image_lib module to aid retrieving image metadata

Copyright (C) 2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# standard library
import sys
import codecs

try:
    import gi
    gi.require_version('GExiv2', '0.10')
    from gi.repository import GExiv2
    HAVE_GEXIV2 = True
except ImportError:
    HAVE_GEXIV2 = False

# local library
from inkex import debug
from image_lib import common as modimg


# globals
WAND_EXIF_PREFIX = "exif:"
USE_GEXIV2 = (True if HAVE_GEXIV2 else False)


def showme(msg):
    """Print debug output only if global DEBUG variable is set."""
    if modimg.DEBUG:
        debug(msg)


def check_gexiv2(use):
    """Enable GExiv2 if present based on runtime check."""
    # pylint: disable=global-statement
    global USE_GEXIV2
    USE_GEXIV2 = (True if use and HAVE_GEXIV2 else False)
    showme('Using GExiv2: {}'.format(USE_GEXIV2))


# ----- common utils -----

def strip_prefix(string, separator, count):
    """Strip *count* prefixes of *string* with *separator* as delimiter."""
    if count > 0:
        alist = string.split(separator)
        if len(alist[count:]):
            return separator.join(alist[count:])
    return string


def strip_exif_prefix(tag):
    """Strip known prefixes from Exif tag name."""
    if USE_GEXIV2:
        return strip_prefix(tag, '.', 2)
    elif modimg.USE_WAND:
        return strip_prefix(tag, ':', 1)
    elif modimg.USE_PIL:
        pass
    return tag


def strip_iptc_prefix(tag):
    """Strip known prefixes from IPTC tag name."""
    if USE_GEXIV2:
        return strip_prefix(tag, '.', 2)
    elif modimg.USE_WAND:
        pass
    elif modimg.USE_PIL:
        pass
    return tag


def strip_xmp_prefix(tag):
    """Strip known prefixes from XMP tag name."""
    if USE_GEXIV2:
        return strip_prefix(tag, '.', 1)
    elif modimg.USE_WAND:
        pass
    elif modimg.USE_PIL:
        pass
    return tag


def strip_tag_prefix(tag_type, tag):
    """Strip prefixes (Wand, Exiv2) from tag names."""
    if tag_type == 'exif':
        return strip_exif_prefix(tag)
    elif tag_type == 'iptc':
        return strip_iptc_prefix(tag)
    elif tag_type == 'xmp':
        return strip_xmp_prefix(tag)
    return tag


def trim_prefixes(separator, key1, key2):
    """Compare prefix count and strip both keys to minimal length."""
    count1 = len(key1.split(separator))
    count2 = len(key2.split(separator))
    if count1 > count2:
        key1 = strip_prefix(key1, separator, count1 - count2)
    elif count2 > count1:
        key2 = strip_prefix(key2, separator, count2 - count1)
    return (key1, key2)


def match_exif_keys(key, tag):
    """Compare Exif image and user key after stripping prefixes."""
    if USE_GEXIV2:
        key, tag = trim_prefixes('.', key, tag)
    elif modimg.USE_WAND:
        key, tag = trim_prefixes(':', key, tag)
    elif modimg.USE_PIL:
        pass
    # showme('key: {}; tag: {}'.format(key, tag))
    return key == tag


def match_iptc_keys(key, tag):
    """Compare IPTC image and user key."""
    if USE_GEXIV2:
        key, tag = trim_prefixes('.', key, tag)
    elif modimg.USE_WAND:
        pass
    elif modimg.USE_PIL:
        pass
    # showme('key: {}; tag: {}'.format(key, tag))
    return key == tag


def match_xmp_keys(key, tag):
    """Compare XMP image and user key."""
    if USE_GEXIV2:
        key, tag = trim_prefixes('.', key, tag)
    elif modimg.USE_WAND:
        pass
    elif modimg.USE_PIL:
        pass
    # showme('key: {}; tag: {}'.format(key, tag))
    return key == tag


def match_keys(tag_type, key, tag):
    """Compare key and tag after stripping prefixes."""
    if tag_type == 'exif':
        return match_exif_keys(key, tag)
    elif tag_type == 'iptc':
        return match_iptc_keys(key, tag)
    elif tag_type == 'xmp':
        return match_xmp_keys(key, tag)
    else:
        return False


# ----- GExiv2 helper -----

def get_gexiv2_metadata(data):
    """Retrieve image metadata via GExiv2 based on image path or buffer."""
    metadata = GExiv2.Metadata()
    if 'path' in data:  # linked image
        metadata.open_path(data['path'])
    elif 'buf' in data:  # embedded image
        metadata.open_buf(data['buf'])
    return metadata


# ----- Exif -----

def read_exif_gexiv2(data):
    """Return Exif metadata retrieved with GExiv2."""
    exifdata = {}
    metadata = get_gexiv2_metadata(data)
    if metadata.has_exif():
        for key in metadata.get_exif_tags():
            tag = key
            exifdata[tag] = metadata.get_tag_interpreted_string(key)
    else:
        showme("Image has no Exif metadata.")
    return exifdata


def read_exif_wand(img):
    """Return Exif metadata retrieved with Wand (ImageMagick)."""
    exifdata = {}
    if hasattr(img, 'metadata'):
        for key, value in img.metadata.items():
            if key.startswith(WAND_EXIF_PREFIX):
                tag = key
                # TODO: value formatting for Wand?
                exifdata[tag] = value
    return exifdata


def read_exif_pil(img):
    """Return Exif metadata retrieved with PIL/Pillow."""
    exifdata = {}
    if hasattr(img, '_getexif'):
        exif = img._getexif()
        if exif is not None:
            for key, value in exif.items():
                # TODO: support splitting GPSInfo ?
                if modimg.PIL_EXIF_TAGS and key in modimg.PIL_EXIF_TAGS.keys():
                    tag = modimg.PIL_EXIF_TAGS.get(key)
                    # TODO: more value formatting for PIL?
                    if isinstance(value, str):
                        tagval = tuple(ord(c) for c in value)
                    else:
                        tagval = value
                    exifdata[tag] = tagval
    return exifdata


def read_exif(img_node):
    """Retrieve Exif metadata, prefer GExiv2 as module if present."""
    if USE_GEXIV2:
        data = modimg.get_image_path_or_buffer(img_node)
        return read_exif_gexiv2(data)
    elif modimg.USE_WAND:
        return read_exif_wand(modimg.get_image(img_node))
    elif modimg.USE_PIL:
        return read_exif_pil(modimg.get_image(img_node))
    else:
        raise RuntimeError(modimg.NO_MODULE)


# ----- IPTC -----

def read_iptc_gexiv2(data):
    """Return IPTC metadata retrieved with GExiv2."""
    iptcdata = {}
    iptcskip = []
    metadata = get_gexiv2_metadata(data)
    if metadata.has_iptc():
        iptcskip.append('Iptc.Envelope.CharacterSet')
        for key in metadata.get_iptc_tags():
            tag = key
            value = metadata.get_tag_interpreted_string(key)
            if tag not in iptcskip:
                iptcdata[tag] = value
            else:
                # FIXME: Exiv2's IPTC strings may contain control characters
                # or use (unknown) encoding ...
                # example: Iptc.Envelope.CharacterSet: 0x1B2547
                #
                # Who's task is it to detect characterset for strings?
                # https://github.com/drewnoakes/metadata-extractor/issues/12
                showme('Skipping \'{}\' ...'.format(tag))
                showme('{}: {}'.format(tag, type(value)))
                if sys.version_info[1] < 3:
                    hexval = value.encode('hex')
                else:
                    # FIXME python3: works, but unlikely correct for:
                    # LookupError: 'hex' is not a text encoding; \
                    #   use codecs.encode() to handle arbitrary codecs
                    hexval = codecs.encode(value.encode(), 'hex')
                showme('{}: 0x{}'.format(tag, hexval))
    else:
        showme("Image has no IPTC metadata.")
    return iptcdata


def read_iptc_wand(img):
    """Return IPTC metadata retrieved with Wand (ImageMagick)."""
    # pylint: disable=unused-argument
    debug("IPTC metadata via Wand not implemented.")
    return {}


def read_iptc_pil(img):
    """Return IPTC metadata retrieved with PIL/Pillow."""
    # pylint: disable=unused-argument
    debug("IPTC metadata via PIL/Pillow not implemented.")
    return {}


def read_iptc(img_node):
    """Retrieve IPTC metadata, prefer GExiv2 as module if present."""
    if USE_GEXIV2:
        data = modimg.get_image_path_or_buffer(img_node)
        return read_iptc_gexiv2(data)
    elif modimg.USE_WAND:
        return read_iptc_wand(modimg.get_image(img_node))
    elif modimg.USE_PIL:
        return read_iptc_pil(modimg.get_image(img_node))
    else:
        raise RuntimeError(modimg.NO_MODULE)


# ----- XMP -----

def read_xmp_gexiv2(data):
    """Return XMP metadata retrieved with GExiv2."""
    xmpdata = {}
    metadata = get_gexiv2_metadata(data)
    if metadata.has_xmp():
        for key in metadata.get_xmp_tags():
            tag = key
            xmpdata[tag] = metadata.get_tag_interpreted_string(key)
    else:
        showme("Image has no XMP metadata.")
    return xmpdata


def read_xmp_wand(img):
    """Return XMP metadata retrieved with Wand (ImageMagick)."""
    # pylint: disable=unused-argument
    debug("XMP metadata via Wand not implemented.")
    return {}


def read_xmp_pil(img):
    """Return XMP metadata retrieved with PIL/Pillow."""
    # pylint: disable=unused-argument
    debug("XMP metadata via PIL/Pillow not implemented.")
    return {}


def read_xmp(img_node):
    """Retrieve XMP metadata, prefer GExiv2 as module if present."""
    if USE_GEXIV2:
        data = modimg.get_image_path_or_buffer(img_node)
        return read_xmp_gexiv2(data)
    elif modimg.USE_WAND:
        return read_xmp_wand(modimg.get_image(img_node))
    elif modimg.USE_PIL:
        return read_xmp_pil(modimg.get_image(img_node))
    else:
        raise RuntimeError(modimg.NO_MODULE)


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
