#!/usr/bin/env python
"""
image_lib.transform - Transform-related utility functions for
                      image-modifying extensions

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=invalid-name

# local library
import inkex
import simpletransform as st
import cubicsuperpath as csp


# legacy (for testing with Inkscape 0.91)

def invertTransform(mat):
    """Return inverted mat."""
    # pylint: disable=invalid-name
    det = mat[0][0]*mat[1][1] - mat[0][1]*mat[1][0]
    if det != 0:  # det is 0 only in case of 0 scaling
        # invert the rotation/scaling part
        a11 = mat[1][1]/det
        a12 = -mat[0][1]/det
        a21 = -mat[1][0]/det
        a22 = mat[0][0]/det
        # invert the translational part
        a13 = -(a11*mat[0][2] + a12*mat[1][2])
        a23 = -(a21*mat[0][2] + a22*mat[1][2])
        return [[a11, a12, a13], [a21, a22, a23]]
    else:
        return [[0, 0, -mat[0][2]], [0, 0, -mat[1][2]]]


# -----

def ident_mat():
    """Return 2x3 identity matrix."""
    return [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]


def invert(mat):
    """Return inverted transformation matrix."""
    if hasattr(st, 'invertTransform'):
        return st.invertTransform(mat)
    else:
        return invertTransform(mat)


def compose_doublemat(mat1, mat2):
    """Compose two mats into a single one, return mat."""
    return st.composeTransform(mat1, mat2)


def compose_triplemat(mat1, mat2, mat3):
    """Compose three mats into a single one, return mat."""
    if mat3 is None:
        mat = mat2
    elif mat2 is None:
        mat = mat3
    else:
        mat = compose_doublemat(mat3, mat2)
    if mat1 is None:
        return mat
    else:
        return compose_doublemat(mat, mat1)


def apply_to(mat, obj):
    """Call applyTransformTo{Path,Node} depending on obj type."""
    # pylint: disable=protected-access
    if isinstance(obj, list):
        st.applyTransformToPath(mat, obj)
    elif isinstance(obj, inkex.etree._Element):
        st.applyTransformToNode(mat, obj)


def apply_to_d(mat, path):
    """Apply transformation matrix to path data."""
    path_d = csp.parsePath(path.get('d'))
    apply_to(mat, path_d)
    path.set('d', csp.formatPath(path_d))


# -----

def rotate(alpha, cx=0, cy=0):
    """Return transformation matrix to rotate by angle, cx, cy."""
    if alpha != 0:
        return st.parseTransform('rotate({0}, {1}, {2})'.format(alpha, cx, cy))
    else:
        return ident_mat()


def scale(fx, fy):
    """Return transformation matrix to scale by fx, fy."""
    if fx != 1 or fy != 1:
        return st.parseTransform('scale({0}, {1})'.format(fx, fy))
    else:
        return ident_mat()


def skew_x(alpha=0):
    """Return transformation matrix to skew by alpha along x-axis"""
    if alpha != 0:
        return st.parseTransform('skewX({0})'.format(alpha))
    else:
        return ident_mat()


def skew_y(alpha=0):
    """Return transformation matrix to skew by alpha along y-axis."""
    if alpha != 0:
        return st.parseTransform('skewY({0})'.format(alpha))
    else:
        return ident_mat()


def translate(x=0, y=0):
    """Return transformation matrix to translate by x, y."""
    if x != 0 or y != 0:
        return st.parseTransform('translate({0}, {1})'.format(x, y))
    else:
        return ident_mat()


# -----

def copy_from(node):
    """Return transformation matrix for node's preserved transform."""
    return st.parseTransform(node.get('transform'))


def absolute(node):
    """Return transformation matrix transforming node to SVGRoot."""
    if node.getparent() is not None:
        return st.composeParents(node, ident_mat())
    else:
        return copy_from(node)


def absolute_diff(node1, node2):
    """Return transformation matrix to transform node2 to node1 coords."""
    mat1 = absolute(node1.getparent())
    mat2 = absolute(node2.getparent())
    return compose_doublemat(invert(mat1), mat2)


def offset(node):
    """Return transformation matrix to apply node offset (x, y)."""
    x, y = [float(i) for i in (node.get('x', 0), node.get('y', 0))]
    return translate(x, y)


# -----

def apply_copy_from(source, target):
    """Apply preserved transform from source to target."""
    mat = copy_from(source)
    apply_to(mat, target)


def apply_absolute(node):
    """Transform node to SVGRoot coords."""
    mat = absolute(node)
    apply_to(mat, node)


def apply_absolute_diff(node1, node2):
    """Transform node2 to node1 coords."""
    mat = absolute_diff(node1, node2)
    apply_to(mat, node2)


def apply_scale(target, fx_fy):
    """Apply scale to target."""
    fx, fy = fx_fy
    mat = scale(fx, fy)
    apply_to(mat, target)


def apply_offset(source, target):
    """Apply source offset (x, y) to target."""
    mat = offset(source)
    apply_to(mat, target)


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
