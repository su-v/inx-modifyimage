NAME = inx-modifyimage

docs = \
       COPYING \
       README \
       TODO

inx = \
      src/image_affine.inx \
      src/image_affine.py \
      src/image_attribs.inx \
      src/image_attribs.py \
      src/image_barrel.inx \
      src/image_barrel.py \
      src/image_crop.inx \
      src/image_crop.py \
      src/image_crop_clip.inx \
      src/image_crop_clip.py \
      src/image_debug.inx \
      src/image_debug.py \
      src/image_freeform.inx \
      src/image_freeform.py \
      src/image_metadata.inx \
      src/image_metadata.py \
      src/image_perspective.inx \
      src/image_perspective.py \
      src/image_perspective_8pt.inx \
      src/image_perspective_8pt.py \
      src/image_polar.inx \
      src/image_polar.py \
      src/image_polynomial.inx \
      src/image_polynomial.py \
      src/image_qp_conformal.inx \
      src/image_qp_conformal.py \
      src/image_reset.inx \
      src/image_reset.py \
      src/image_slice.inx \
      src/image_slice.py

modules = \
	  src/image_lib/__init__.py \
	  src/image_lib/common.py \
	  src/image_lib/geom.py \
	  src/image_lib/metadata.py \
	  src/image_lib/transform.py

data =

# -----

source = $(inx) $(modules)

ChangeLog.txt : $(docs) $(source) $(data)
	rm -f $@
	git log --oneline > $@

all = ChangeLog.txt $(docs) $(source) $(data)

revno = $(shell git rev-parse --short HEAD)
tag = $(shell git tag | tail -1)

.PHONY:	list
list:	$(all)
	@for i in $(all); do \
		echo $$i; \
	done

.PHONY:	zip
zip:	$(all)
	zip "$(NAME)-$(revno).zip" -X $(all)
	@echo "$(NAME)-$(revno).zip"

.PHONY: release
release: \
	$(all)
	zip "$(NAME)-$(tag).zip" -X $(all)
	@echo "$(NAME)-$(tag).zip"
