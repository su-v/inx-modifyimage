inx-modifyimage
===============

A set of extensions to modify bitmap images in Inkscape.

Basic operations:
- display image info
- render image metadata
- adjust size of SVG <image> element
- crop
- affine, polar, barrel distortion
- perspective, envelope distortion
- polynomial distortion
- quadratic conformal distortion
- slice


Requirements
============

The python-based script extensions use Imaging modules to process and
modify the bitmap images:
- PIL/Pillow (any version)
- Wand (>= 4.1)

Default Imaging module is Wand if available, otherwise PIL/Pillow. The
imaging module can be switched at runtime if both are available.

Operations supported with PIL/Pillow:
- Bitmap Image Info
- Set Image Scale
- Crop
- Crop to Clip
- Perspective
- Perspective Correction
- Quadratic Confirmal Distortion
- Slice

Operations supported with Wand (ImageMagick):
- Bitmap Image Info
- Set Image Scale
- Affine Distortion
- Barrel Distortion
- Crop
- Crop to Clip
- Envelope
- Perspective
- Perspective Correction
- Polar Distortion
- Shepards Distortion
- Polynomial Distortions
- Slice

Image Metadata:
Extracting metadata is optionally handled with GExiv2.


Basic Usage
===========

Import a bitmap image, draw a quadrilateral path on-top of the image,
select both and apply 'Modify Image > Perspective/Envelope'.


Input and Scope
===============

The modifications applied to the selected image(s) are destructive: an
embedded bitmap image is replaced with the modified image, a link to an
external bitmap image is replaced with an embedded copy of the image
data, modified according to the extension's parameters (the original
external bitmap image is not touched).

Most extensions require a selection of a bitmap image and one or more
helper paths. The helper path requirements vary:
- Crop: a path (no requirements or restrictions)
- Polar Distortion: a path with 3 nodes
- Envelope/Perspective: a quadrilateral path (4 nodes, open or closed)
- Quadratic Conformal Distortion: a path with 8 nodes
- Affine Distortion: 2 paths with 3 nodes each
- Perspective Correction: 2 quadrilateral paths (4 nodes)
- Polynomial Distortion: 2 paths with same node count (>=3)
- Shepards Distortion: 2 paths with same node count
- Slice: 1 or more helper paths

Some extensions support numeric input to modify a selected bitmap image:
- Barrel Distortion
- Polar Distortion
- Slice

Some extensions support different scopes: selected images only, all
images in the selection (e.g. inside nested groups), all images in the
document (including those in the <defs>). Such scopes are available for
- Image Metadata
- Set Image Attributes 2
- Set Image Scale
- Crop to Clip


Options specific to Imaging module
==================================

Extensions which use ImageMagick via Wand allow to define 'Virtual
Pixel' and 'Matte Color' (used for pixels not defined in the source
image).


Output
======

Some extensions allow to 'fit' the output image to the helper path
in position and size: Perspective/Envelope, Polar, Quadratic Conformal.

Most extensions offer options to "wrap" the output image. The modified
image can be clipped or grouped with the helper path, or the helper path
can be deleted.


Files and Installation
======================

Copy all *.py and *.inx files as well as the sub-directory 'image_lib/'
from 'src/' to the extensions directory in the Inkscape user config
directory:
- Linux/OS X:   ~/.config/inkscape/extensions
- Windows:      %APPDATA%\inkscape\extensions
and relaunch Inkscape.

The installed extensions will be available as:

Extensions > Debug:
- Bitmap Image Info

Extensions > Images:
- Image Metadata
- Set Image Attributes 2
- Set Image Scale

Extensions > Modify Image:
- Affine Distortion
- Barrel Distortion
- Crop
- Crop to Clip
- Perspective Correction
- Perspective/Envelope
- Polar Distortion
- Polynomial Distortion
- Quadratic Conformal
- Shepards Distortion
- Slice


Source
======

The extensions are developed and maintained in:
https://gitlab.com/su-v/inx-modifyimage

A ZIP archive of recent snapshot can also be downloaded here:
https://gitlab.com/su-v/inx-modifyimage/tags

The feature request to include these extensions in inkscape:
https://bugs.launchpad.net/inkscape/+bug/1516357

Examples (GitLab Wiki):
https://gitlab.com/su-v/inx-modifyimage/wikis/home


License
=======

Same as Inkscape (GPL-2+).
